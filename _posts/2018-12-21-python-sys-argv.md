---
layout: post
title: Python Argument with sys.argv
categories: [python]
view: true
---  
**use sys.argv in Python**

`example1`
```python
import sys
x = sys.argv
print x
```
`output`
```sh
sauron@shadow:~$ python ar.py  1 2 3
['ar.py', '1', '2', '3']
```

`example2`
```python
import sys
print 'script name: {}'.format(sys.argv[0]) 
print 'value 1: {}'.format(sys.argv[1])
print 'value 2: {}'.format(sys.argv[2])
```
`output`  
```sh
sauron@shadow:~$ python sc2.py phi hung
script name: sc2.py
value 1: phi
value 2: hung
```
`example3`
```python
import sys
print 'all values without script name: {}'.format(sys.argv[1:])
```
`output`
```sh
sauron@shadow:~$ python sc3.py phi hung
all values without script name: ['phi', 'hung']
```


