---
layout: post
title:  View ports/sockets - RHEL 7
categories: [RedHat7]
view: true
---
### lsof
`List all port and state`
```sh
[root@app ~]# lsof -i
COMMAND   PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
dhclient  780   root    6u  IPv4  18706      0t0  UDP *:bootpc
dhclient  782   root    6u  IPv4  18717      0t0  UDP *:bootpc
sshd      963   root    3u  IPv4  19768      0t0  TCP *:EtherNet/IP-1 (LISTEN)
sshd      963   root    4u  IPv6  19777      0t0  TCP *:EtherNet/IP-1 (LISTEN)
master   1133   root   13u  IPv4  20279      0t0  TCP localhost:smtp (LISTEN)
master   1133   root   14u  IPv6  20280      0t0  TCP localhost:smtp (LISTEN)
sshd     2242   root    3u  IPv4  25357      0t0  TCP app.node:EtherNet/IP-1->192.168.113.128:58742 (ESTABLISHED)
httpd    2287   root    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2288 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2289 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2290 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2292 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2299 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2300 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2302 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2303 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2304 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd    2305 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
```
`Check Port`
```sh
[root@app ~]# lsof -i :2222
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd     963 root    3u  IPv4  19768      0t0  TCP *:EtherNet/IP-1 (LISTEN)
sshd     963 root    4u  IPv6  19777      0t0  TCP *:EtherNet/IP-1 (LISTEN)
sshd    2242 root    3u  IPv4  25357      0t0  TCP app.node:EtherNet/IP-1->192.168.113.128:58742 (ESTABLISHED)

[root@app ~]# lsof -i :25
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
master  1133 root   13u  IPv4  20279      0t0  TCP localhost:smtp (LISTEN)
master  1133 root   14u  IPv6  20280      0t0  TCP localhost:smtp (LISTEN)
```
`Check Range Port`
```sh
[root@app ~]# lsof -i TCP:1-1024
```
Grep IPv4, IPv6
```sh
[root@app ~]# lsof -i4
COMMAND   PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
dhclient  780 root    6u  IPv4  18706      0t0  UDP *:bootpc
dhclient  782 root    6u  IPv4  18717      0t0  UDP *:bootpc
sshd      963 root    3u  IPv4  19768      0t0  TCP *:EtherNet/IP-1 (LISTEN)
master   1133 root   13u  IPv4  20279      0t0  TCP localhost:smtp (LISTEN)
sshd     2242 root    3u  IPv4  25357      0t0  TCP app.node:EtherNet/IP-1->192.168.113.128:58742 (ESTABLISHED)

[root@app ~]# lsof -i6
COMMAND  PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd     963   root    4u  IPv6  19777      0t0  TCP *:EtherNet/IP-1 (LISTEN)
master  1133   root   14u  IPv6  20280      0t0  TCP localhost:smtp (LISTEN)
httpd   2287   root    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2288 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2289 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2290 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2292 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2299 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2300 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2302 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2303 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2304 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2305 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
```
`TCP or UDP only`
```sh
[root@app ~]# lsof -iTCP
COMMAND  PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd     963   root    3u  IPv4  19768      0t0  TCP *:EtherNet/IP-1 (LISTEN)
sshd     963   root    4u  IPv6  19777      0t0  TCP *:EtherNet/IP-1 (LISTEN)
master  1133   root   13u  IPv4  20279      0t0  TCP localhost:smtp (LISTEN)
master  1133   root   14u  IPv6  20280      0t0  TCP localhost:smtp (LISTEN)
sshd    2242   root    3u  IPv4  25357      0t0  TCP app.node:EtherNet/IP-1->192.168.113.128:58742 (ESTABLISHED)
httpd   2287   root    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2288 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2289 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2290 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2292 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2299 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2300 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2302 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2303 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2304 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2305 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)

[root@app ~]# lsof -iUDP
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
dhclient 780 root    6u  IPv4  18706      0t0  UDP *:bootpc
dhclient 782 root    6u  IPv4  18717      0t0  UDP *:bootpc
```
`Check state [listen | established]`
```sh
[root@app ~]# lsof -i -s TCP:listen
COMMAND  PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd     963   root    3u  IPv4  19768      0t0  TCP *:EtherNet/IP-1 (LISTEN)
sshd     963   root    4u  IPv6  19777      0t0  TCP *:EtherNet/IP-1 (LISTEN)
master  1133   root   13u  IPv4  20279      0t0  TCP localhost:smtp (LISTEN)
master  1133   root   14u  IPv6  20280      0t0  TCP localhost:smtp (LISTEN)
httpd   2287   root    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2288 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2289 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2290 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2292 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2299 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2300 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2302 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2303 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2304 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
httpd   2305 apache    4u  IPv6  26378      0t0  TCP *:http (LISTEN)
[root@app ~]# lsof -i -s TCP:established
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd    2242 root    3u  IPv4  25357      0t0  TCP app.node:EtherNet/IP-1->192.168.113.128:58742 (ESTABLISHED)
```
`Kill processID of Ports`
```
kill -9 $(lsof -t -i:port)
kill -9 $(lsof -i:port| awk 'END{print $2}')
```
Igrone user, port, so on
```sh
[root@app ~]# lsof -u ^root | tail
httpd     2305      apache  DEL       REG                0,4              32769 /SYSV0114671a
httpd     2305      apache    0r      CHR                1,3      0t0      6221 /dev/null
httpd     2305      apache    1u     unix 0xffff9cb616846800      0t0     26368 socket
httpd     2305      apache    2w      REG              253,0    15983 101124207 /var/log/httpd/error_log
httpd     2305      apache    3u     sock                0,7      0t0     26377 protocol: TCP
httpd     2305      apache    4u     IPv6              26378      0t0       TCP *:http (LISTEN)
httpd     2305      apache    5r     FIFO                0,9      0t0     26393 pipe
httpd     2305      apache    6w     FIFO                0,9      0t0     26393 pipe
httpd     2305      apache    7w      REG              253,0    33567 101124208 /var/log/httpd/access_log
httpd     2305      apache    8u  a_inode               0,10        0      6217 [eventpoll]
```

### netstat
```sh
[root@app ~]# netstat -antpu
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:2222            0.0.0.0:*               LISTEN      963/sshd
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      1133/master
tcp        0     36 192.168.113.129:2222    192.168.113.1:49971     ESTABLISHED 2497/sshd: root@pts
tcp6       0      0 :::2222                 :::*                    LISTEN      963/sshd
tcp6       0      0 :::80                   :::*                    LISTEN      2287/httpd
tcp6       0      0 ::1:25                  :::*                    LISTEN      1133/master
udp        0      0 0.0.0.0:68              0.0.0.0:*                           782/dhclient
udp        0      0 0.0.0.0:68              0.0.0.0:*                           780/dhclient
```
`Options:`
```
-a: which state are listen or established
-n: no reslove hostname
-t: protocol TCP, t4 -> TCPv4, t6 -> TCPv6
-p: PID
-u: protocol UDP 
```
```sh
[root@app ~]# netstat -ie
Kernel Interface table
ens35: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.113.129  netmask 255.255.255.0  broadcast 192.168.113.255
        inet6 fe80::e4f8:8551:514c:a098  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:3e:6f:68  txqueuelen 1000  (Ethernet)
        RX packets 3630  bytes 308966 (301.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2402  bytes 420530 (410.6 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

ens36: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.69.131  netmask 255.255.255.0  broadcast 172.16.69.255
        inet6 fe80::b4f5:76c9:fa7d:4182  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:3e:6f:72  txqueuelen 1000  (Ethernet)
        RX packets 465  bytes 389816 (380.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 304  bytes 29595 (28.9 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 20  bytes 1780 (1.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 20  bytes 1780 (1.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

### ss
`Options:`
```
-a: all state
	1. established
	2. syn-sent
	3. syn-recv
	4. fin-wait-1
	5. fin-wait-2
	6. time-wait
	7. closed
	8. close-wait
	9. last-ack
	10. closing
	11. all - All of the above states
	12. connected - All the states except for listen and closed
	13. synchronized - All the connected states except for syn-sent
	14. bucket - Show states, which are maintained as minisockets, i.e. time-wait and syn-recv.
	15. big - Opposite to bucket state.
-n: Don't reslove hostname
-t: TCP
-u: UDP
-l: state listen
-p: processID 
-o: time information of each connection
-6: TCPv6
-4: TCPv4

<= or le : Less than or equal to port
>= or ge : Greater than or equal to port
== or eq : Equal to port
!= or ne : Not equal to port
< or gt : Less than to port
> or lt : Greater than to port

```
`List all state tcp/udp port`
```sh
[root@app ~]# ss -antu
Netid  State      Recv-Q Send-Q                                     Local Address:Port                                                    Peer Address:Port
udp    UNCONN     0      0                                                      *:68                                                                 *:*

udp    UNCONN     0      0                                                      *:68                                                                 *:*

tcp    LISTEN     0      128                                                    *:2222                                                               *:*

tcp    LISTEN     0      100                                            127.0.0.1:25                                                                 *:*

tcp    ESTAB      0      36                                       192.168.113.129:2222                                                   192.168.113.1:49971

tcp    LISTEN     0      128                                                   :::2222                                                              :::*

tcp    LISTEN     0      128                                                   :::80                                                                :::*

tcp    LISTEN     0      100                                                  ::1:25                                                                :::*
```
`List all state tcp/udp port  with ProcessID`
```
[root@app ~]# ss -antup
Netid  State      Recv-Q Send-Q                                     Local Address:Port                                                    Peer Address:Port
udp    UNCONN     0      0                                                      *:68                                                                 *:*
 users:(("dhclient",pid=2846,fd=6))
udp    UNCONN     0      0                                                      *:68                                                                 *:*
 users:(("dhclient",pid=780,fd=6))
tcp    LISTEN     0      128                                                    *:2222                                                               *:*
 users:(("sshd",pid=963,fd=3))
tcp    LISTEN     0      100                                            127.0.0.1:25                                                                 *:*
 users:(("master",pid=1133,fd=13))
tcp    ESTAB      0      36                                       192.168.113.129:2222                                                   192.168.113.1:49971
 users:(("sshd",pid=2497,fd=3))
tcp    LISTEN     0      128                                                   :::2222                                                              :::*
 users:(("sshd",pid=963,fd=4))
tcp    LISTEN     0      128                                                   :::80                                                                :::*
 users:(("httpd",pid=2305,fd=4),("httpd",pid=2304,fd=4),("httpd",pid=2303,fd=4),("httpd",pid=2302,fd=4),("httpd",pid=2300,fd=4),("httpd",pid=2299,fd=4),("httpd",pid=2292,fd=4),("httpd",pid=2290,fd=4),("httpd",pid=2289,fd=4),("httpd",pid=2288,fd=4),("httpd",pid=2287,fd=4))
tcp    LISTEN     0      100                                                  ::1:25                                                                :::*
 users:(("master",pid=1133,fd=14))
```
`Time information of each connection`
```sh
[root@app ~]# ss -antuo
Netid  State      Recv-Q Send-Q                                     Local Address:Port                                                    Peer Address:Port
udp    UNCONN     0      0                                                      *:68                                                                 *:*

udp    UNCONN     0      0                                                      *:68                                                                 *:*

tcp    LISTEN     0      128                                                    *:2222                                                               *:*

tcp    LISTEN     0      100                                            127.0.0.1:25                                                                 *:*

tcp    ESTAB      0      36                                       192.168.113.129:2222                                                   192.168.113.1:49971
 timer:(on,241ms,0)
tcp    LISTEN     0      128                                                   :::2222                                                              :::*

tcp    LISTEN     0      128                                                   :::80                                                                :::*

tcp    LISTEN     0      100                                                  ::1:25                                                                :::*
```
`Display all Ipv4 tcp sockets that are in "connected" state`
```sh
[root@app ~]# ss -t4 state established
Recv-Q Send-Q                                           Local Address:Port                                                            Peer Address:Port
0      36                                             192.168.113.129:EtherNet/IP-1                                                  192.168.113.1:49971
```
`Display all socket connections with source or destination port of ssh.`
```sh
[root@app ~]# ss -atn dport = :49971
State      Recv-Q Send-Q                                        Local Address:Port                                                       Peer Address:Port
ESTAB      0      36                                          192.168.113.129:2222                                                      192.168.113.1:49971

[root@app ~]# ss -atn sport = :80 or sport = :2222
State      Recv-Q Send-Q                                        Local Address:Port                                                       Peer Address:Port
LISTEN     0      128                                                       *:2222                                                                  *:*
ESTAB      0      36                                          192.168.113.129:2222                                                      192.168.113.1:49971
LISTEN     0      128                                                      :::2222                                                                 :::*
LISTEN     0      128                                                      :::80                                                                   :::*
```
`Sockets with destination port`
```sh
[root@app ~]# ss -ant src :2222
State       Recv-Q Send-Q                        Local Address:Port                                       Peer Address:Port
LISTEN      0      128                                       *:2222                                                  *:*
ESTAB       0      36                          192.168.113.129:2222                                      192.168.113.1:49440
LISTEN      0      128                                      :::2222                                                 :::*
```
