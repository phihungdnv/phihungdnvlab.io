---
layout: post
title: Paramiko SSH through proxycommand
categories: [python]
view: true
--- 

# SOURCE CODES  

```python
import paramiko
import sys
import os
import csv
from termcolor import colored
import time

class jenkins:
    def __init__(self,apps):
        conf = paramiko.SSHConfig()
        conf.parse(open(os.path.expanduser('/home/itson/.ssh/config')))
        host = conf.lookup(apps)
        self.hostname =  host['hostname']
        self.username = host['user']
        self.pkey = host['identityfile']
        self.sock = paramiko.ProxyCommand(host.get('proxycommand'))

    def connect_to_server(self):
        try:
            print colored('Connecting to %s','red')%self.hostname
            self.client = paramiko.SSHClient()
            self.client.load_system_host_keys()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(hostname=self.hostname,username=self.username,key_filename=self.pkey,sock=self.sock)
            return True
        except Exception as e:
            print 'Connection Failed'
            print e


    def execute_command(self):
        try:
            if self.connect_to_server():
                commands = ['ls -l','sudo ip a']
                for i in range(len(commands)):
                    print colored('Executing command --> %s','green')%commands[i]
                    stidin,stdout,stderr = self.client.exec_command(commands[i])
                    self.stdout = stdout.read()
                    self.stderr = stderr.read()
                    if self.stderr:
                        print colored("failed --> %s","yellow")%commands[i]
                        print colered('%s','red')%self.stderr
                        self.client.close()
                    else:                        
                        print self.stdout
                        print colored('Completed, --> next steps','yellow')
                        #self.client.close()
            else:
                print colored('Could not establish SSH connection')
        except Exception as e:
                print colored('Could not establish SSH connection')
                print e
if __name__=='__main__':
    #connect_to_client("jenkins01.di01.dev")
    obj = jenkins('jenkins01.di01.dev')
    obj.execute_command()
```
# OUTPUT  

```sh
[itson@jenkins jenkins-python-dev]$ python jenkins_dev_class.py 
Connecting to 10.92.17.29
Executing command --> ls -l
Completed, --> next steps
total 149096
-rwxrwxrwx 1 itson itson      2767 Nov 17 19:07 appNodes.json
-rw-r--r-- 1 root  root    1661676 Nov 17 19:15 get-pip.py
-rw-rw-r-- 1 itson itson   3312061 Aug 13 16:50 jenkins-cli.jar
drwxr-xr-x 5 itson itson      4096 Nov  9 14:16 monitoring
-rwxrwxr-x 1 itson itson 147675910 Nov 11 20:01 operator-platform-api-1.0.0-SNAPSHOT.war
-rw-rw-r-- 1 itson itson      1061 Nov 17 13:30 pyssh.py
drwxr-xr-x 3 itson itson      4096 Nov 10 10:36 status-check

Executing command --> sudo ip a
Completed, --> next steps
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP qlen 1000
    link/ether 00:0c:29:db:21:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.92.17.29/25 brd 10.92.17.127 scope global eth0
    inet6 fe80::20c:29ff:fedb:21e7/64 scope link 
       valid_lft forever preferred_lft forever
```
