---
layout: post
title:  Modifying the docker daemon Red Hat Enterprise Linux - DOCKER
categories: [docker]
view: true
tag: docker
---
Docker daemon run by the settings in the `/etc/sysconfig/docker` file. List vailable options:
```sh
$ docker daemon --help
```
**Exposing the docker daemon through a TCP port**
```
OPTIONS='--selinux-enabled --log-driver=journald --signatureverification=false -H tcp://0.0.0.0:2375'
```
This configuration exposes the docker daemon to any requests on the encrypted TCP port 2376 for all external interfaces. Port 2375 can be used for unencrypted communication with the daemon  

**Registry options **
Search for or pull images, the docker command uses the Docker registry.In RHEL and RHEL Atomic Host, this entry in the /etc/sysconfig/docker file causes the Red Hat registry (registry.access.redhat.com) to be used first:   
```
ADD_REGISTRY='--add-registry registry.access.redhat.com'
```
Add registry from https without certificate
```
INSECURE_REGISTRY='--insecure-registry newregistry.example.com'
```
Add a private registry  
```
ADD_REGISTRY='--add-registry registry.access.redhat.com --add-registry myregistry.example.com'
```
Block a registry  
```
BLOCK_REGISTRY='--block-registry docker.io'
```

**User namespaces options**
