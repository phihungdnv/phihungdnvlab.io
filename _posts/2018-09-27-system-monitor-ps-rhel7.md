---
layout: post
title:  System monitoring tools (ps command) - RHEL 7
categories: [RedHat7]
view: true
tag: ps
---
>The `ps` command allows you to display information about running processes.

**To list all processes that are currently running on the system including processes owned by other users, type the following at a shell prompt:**  
`ps ax`  
**To display the owner alongside each process, use the following command:**  
`ps aux`  
**Use the ps command in a combination with grep to see if a particular process is running**  
`ps ax | grep httpd`
