---
layout: post
title:  journalctl debug system log -RHEL 7
categories: [RedHat7]
view: true
---

`journalctl --no-page` -> Reading all log without page`  
`journalctl --no-page -u http.service` -> Read entries for a specific service; `systemctl -at service` to get right name service  
`journalctl --no-page -u http.service --boot` Read entries since boot

**Past Boots**
```sh
mkdir -p /var/log/journal
vi /etc/systemd/journald.conf
...
[Journal]
Storage=persistent
...
```
`journalctl --list-boots` -> List boots
`journalctl -b -1` or `journalctl -b caf0524a1d394ce0bdbcff75b94444fe` -> Display this boot
`Time date`
```sh
[root@app ~]# journalctl --since "2018-09-17 23:05:00"
-- Logs begin at Mon 2018-09-17 22:36:40 +07, end at Mon 2018-09-17 23:13:34 +07. --
Sep 17 23:05:42 app.node dhclient[779]: DHCPREQUEST on ens36 to 172.16.20.254 port 67 (xid=0x4b641f7d)
Sep 17 23:05:42 app.node dhclient[779]: DHCPACK from 172.16.20.254 (xid=0x4b641f7d)
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1360] dhcp4 (ens36):   address 172.16.20.128
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1361] dhcp4 (ens36):   plen 24 (255.255.255.0)
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1361] dhcp4 (ens36):   gateway 172.16.20.2
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1361] dhcp4 (ens36):   lease time 1800
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1361] dhcp4 (ens36):   nameserver '172.16.20.2'
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1362] dhcp4 (ens36):   domain name 'localdomain'
Sep 17 23:05:42 app.node NetworkManager[646]: <info>  [1537200342.1362] dhcp4 (ens36): state changed bound -> bound
Sep 17 23:05:42 app.node dbus[624]: [system] Activating via systemd: service name='org.freedesktop.nm_dispatcher' unit='dbus-org.freedesktop.nm-dispatcher.service'
Sep 17 23:05:42 app.node systemd[1]: Starting Network Manager Script Dispatcher Service...
Sep 17 23:05:42 app.node dhclient[779]: bound to 172.16.20.128 -- renewal in 739 seconds.
Sep 17 23:05:42 app.node dbus[624]: [system] Successfully activated service 'org.freedesktop.nm_dispatcher'
Sep 17 23:05:42 app.node systemd[1]: Started Network Manager Script Dispatcher Service.
Sep 17 23:05:42 app.node nm-dispatcher[1406]: req:1 'dhcp4-change' [ens36]: new request (2 scripts)
Sep 17 23:05:42 app.node nm-dispatcher[1406]: req:1 'dhcp4-change' [ens36]: start running ordered scripts...
Sep 17 23:13:34 app.node systemd-journal[456]: Permanent journal is using 8.0M (max allowed 3.8G, trying to leave 4.0G free of 37.8G available → current limit 3.8G).
Sep 17 23:13:34 app.node systemd-journal[456]: Time spent on flushing to /var is 29.133ms for 2267 entries.
Sep 17 23:13:34 app.node dhclient[777]: DHCPREQUEST on ens35 to 192.168.113.254 port 67 (xid=0x79480e0)
Sep 17 23:13:34 app.node dhclient[777]: DHCPACK from 192.168.113.254 (xid=0x79480e0)
Sep 17 23:13:34 app.node systemd[1]: Starting Network Manager Script Dispatcher Service...
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2943] dhcp4 (ens35):   address 192.168.113.129
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2944] dhcp4 (ens35):   plen 24 (255.255.255.0)
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2944] dhcp4 (ens35):   lease time 1800
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2945] dhcp4 (ens35):   nameserver '192.168.113.1'
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2945] dhcp4 (ens35):   domain name 'localdomain'
Sep 17 23:13:34 app.node NetworkManager[646]: <info>  [1537200814.2945] dhcp4 (ens35): state changed bound -> bound
Sep 17 23:13:34 app.node dbus[624]: [system] Activating via systemd: service name='org.freedesktop.nm_dispatcher' unit='dbus-org.freedesktop.nm-dispatcher.service'
Sep 17 23:13:34 app.node dhclient[777]: bound to 192.168.113.129 -- renewal in 740 seconds.
Sep 17 23:13:34 app.node rsyslogd[972]: imjournal: journal reloaded... [v8.24.0 try http://www.rsyslog.com/e/0 ]
Sep 17 23:13:34 app.node systemd[1]: Started Network Manager Script Dispatcher Service.
Sep 17 23:13:34 app.node dbus[624]: [system] Successfully activated service 'org.freedesktop.nm_dispatcher'
Sep 17 23:13:34 app.node nm-dispatcher[1435]: req:1 'dhcp4-change' [ens35]: new request (2 scripts)
Sep 17 23:13:34 app.node nm-dispatcher[1435]: req:1 'dhcp4-change' [ens35]: start running ordered scripts...
```
`journalctl --since "2015-01-10" --until "2015-01-11 03:00"`
`journalctl --since yesterday`
`journalctl --since 09:00 --until "1 hour ago"`
`journalctl -u nginx.service --since today`