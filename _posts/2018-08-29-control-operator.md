---
layout: post
title: Control operator - Unix/Linux
categories: [linux]
view: true
---
*List terminators*
`;` Will run one command after another has finished, irrespective of the outcome of the first.  
```sh
command1 ; command2
```
First command1 is run, in the foreground, and once it has finished, command2 will be run.  
`&` This will run a command in the background, allowing you to continue working in the same shell.
```sh
command1 & command2
```
command1 is launched in the background and command2 starts running in the foreground immediately, without waiting for command1 to exit.  
*Logical operators*
`&&` Used to build AND lists, it allows you to run one command only if another exited successfully.  
```sh
