---
layout: post
title: Find out what process is using all the RAM?
categories: [SysAdmin]
view: true
---

**Megabyte**
```sh
$ ps aux  | awk '{print $6/1024 " MB\t\t" $11}'  | sort -n | tail -10
1.65625 MB              -bash
1.78906 MB              /usr/sbin/zabbix_agentd:
1.80859 MB              pickup
1.88281 MB              /usr/libexec/sssd/sssd_pam
1.94531 MB              sshd:
2.03125 MB              /usr/libexec/sssd/sssd_nss
5.30859 MB              /sbin/rsyslogd
17.9297 MB              python
38.1484 MB              /usr/libexec/sssd/sssd_be
29612.7 MB              /opt/jdk8/bin/java
```

**Gigabyte**
```sh
$ ps aux  | awk '{print $6/(1024*1024) " GB\t\t" $11}'  | sort -n | tail -10
0.00160217 GB           nimbus(cdm)
0.00162888 GB           -bash
0.00174713 GB           /usr/sbin/zabbix_agentd:
0.0017662 GB            pickup
0.00183868 GB           /usr/libexec/sssd/sssd_pam
0.00189972 GB           sshd:
0.00201797 GB           /usr/libexec/sssd/sssd_nss
0.00518417 GB           /sbin/rsyslogd
0.0372543 GB            /usr/libexec/sssd/sssd_be
29.0302 GB              /opt/jdk8/bin/java

```


