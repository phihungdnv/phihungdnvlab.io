---
layout: post
title: Investigating the docker environment Red Hat Enterprise Linux [4] - DOCKER
categories: [docker]
view: true
tag: docker
---
`docker version`: The version option which versions of different Docker components are installed.
```sh
[rhel7@localhost ~]$ docker version
Client:
 Version:         1.13.1
 API version:     1.26
 Package version: docker-1.13.1-74.git6e3bb8e.el7.x86_64
 Go version:      go1.9.2
 Git commit:      6e3bb8e/1.13.1
 Built:           Wed Aug  1 16:56:40 2018
 OS/Arch:         linux/amd64

Server:
 Version:         1.13.1
 API version:     1.26 (minimum version 1.12)
 Package version: docker-1.13.1-74.git6e3bb8e.el7.x86_64
 Go version:      go1.9.2
 Git commit:      6e3bb8e/1.13.1
 Built:           Wed Aug  1 16:56:40 2018
 OS/Arch:         linux/amd64
 Experimental:    false
```
`docker info`: Locations of different components, such as how many local container and image there are, as well as information on the size and location of Docker storage areas.
```sh
[rhel7@localhost ~]$ docker info
Containers: 4
 Running: 0
 Paused: 0
 Stopped: 4
Images: 3
Server Version: 1.13.1
Storage Driver: overlay2
 Backing Filesystem: xfs
 Supports d_type: true
 Native Overlay Diff: true
Logging Driver: journald
Cgroup Driver: systemd
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Authorization: rhel-push-plugin
Swarm: inactive
Runtimes: docker-runc runc
Default Runtime: docker-runc
Init Binary: /usr/libexec/docker/docker-init-current
containerd version:  (expected: aa8187dbd3b7ad67d8e5e3a15115d3eef43a7ed1)
runc version: 5eda6f6fd0c2884c2c8e78a6e7119e8d0ecedb77 (expected: 9df8b306d01f59d3a8029be411de015b7304dd8f)
init version: fec3683b971d9c3ef73f284f176672c44b448662 (expected: 949e6facb77383876aeff8a6944dde66b3089574)
Security Options:
 seccomp
  WARNING: You're not using the default seccomp profile
  Profile: /etc/docker/seccomp.json
 selinux
Kernel Version: 3.10.0-862.11.6.el7.x86_64
Operating System: Red Hat Enterprise Linux
OSType: linux
Architecture: x86_64
Number of Docker Hooks: 3
CPUs: 2
Total Memory: 3.685 GiB
Name: localhost.localdomain
ID: Q6VU:7IXD:26O5:NFTB:CECO:MMEZ:T5GP:NPVI:M3LQ:Q3Q2:NDHC:V7AA
Docker Root Dir: /var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Registry: https://registry.access.redhat.com/v1/
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false
Registries: registry.access.redhat.com (secure), docker.io (secure)
[rhel7@localhost ~]$
```