---
layout: post
title: Unix / Linux - Shell Quoting Mechanisms
categories: [linux]
view: true
---

*Back quote \`command_executed`*  
Example:
```sh
root@pentest:~# echo `ip a | grep ens4`
2: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1460 qdisc pfifo_fast state UP group default qlen 1000 inet 10.148.0.2/32 brd 10.148.0.2 scope global ens4
```
*Start of a line or a string (`^`)*
Match any line or string which start with a pattern
Example:
```sh
root@pentest:~# ls | grep ^li
libubox
libuwsc
list.py
```
*Dollar Symbol (`$`)*
Match any line or string ending with a pattern
Example:
```sh
root@pentest:~# ll | grep py$
-rw-r--r--  1 root root     1016 Jul 18 09:15 acd.py
-rw-r--r--  1 root root      459 Jul 18 09:09 ac.py
-rw-r--r--  1 root root     1420 Jul 18 14:26 adc.py
```
Command Substitution (similar \`command_executed`)
```sh
[root@con01.di01.sapp ~]$ for h in $(knife node list | grep ^sapp-payment-adapter | sort); do echo $h; done
sapp-payment-adapter01.di01.sapp
sapp-payment-adapter02.di01.sapp
sapp-payment-adapter03.di01.sapp
```
*Command Separators*
`;`
This line executes x, y, and z one by one.
```sh
$ x; y; z
```
`|`
A pipe is a separator.
```sh
$ x | y | z 
```
This line consists of three separate tasks. In the first job, the shell redirects standard output of the task x to standard input of task y and redirects y's output to z's standard input.
`&`
A pipe is a separator.
```sh
$ x & y & z 
```Learning Linux Shell Scripting
This line consists of three separate tasks. The tasks x and y will run in background, but z task will run as a foreground job.