---
layout: post
title: GITLAB MARKDOWN
categories: [git]
description: gitlab doccument
---
### Newline
Ngo Phi Hung  [followed by two or more spaces]  
Networking Linux  
Python programing  
### Blockquote
```no-higlight
> Single blockqoute 
[enter to newline]
```
> ngo phi hung
  
```no-higlight
>>>
multiple blockqoute [followed by two or more spaces]
multiple blockqoute

multiple blockqoute
>>>
```
>>>
multiple blockqoute  
multiple blockqoute  

multiple blockqoute  
>>>

```no-higlight
> multiple blockqoute
multiple blockqoute [followed by two or more spaces]
multiple blockqoute
multiple blockqoute

```
> multiple blockqoute  
multiple blockqoute	  
multiple blockqoute	     
multiple blockqoute     
    
### Code and Syntax Highlighting
Blocks of code are either fenced by lines with three back-ticks <code>```</code>
Example:  

	```javascript
    var s = "JavaScript syntax highlighting";
    alert(s);
    ```

    ```python
    def function():
        #indenting works just fine in the fenced code block
        s = "Python syntax highlighting"
        print s
    ```

    ```ruby
    require 'redcarpet'
    markdown = Redcarpet.new("Hello World!")
    puts markdown.to_html
    ```
Output:
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
	#indenting works just fine in the fenced code block
	s = "Python syntax highlighting"
	print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```
### Inline Diff
```
With inline diffs tags you can display {+ additions +} or [- deletions -].
The wrapping tags can be either curly braces or square brackets: [+ additions +] or {- deletions -}.
```
Output:  
With inline diffs tags you can display {+ additions +} or [- deletions -] or {+ ngo +}, {+ phi +}, {- hung -}  
The wrapping tags can be either curly braces or square brackets: [+ additions +] or {- deletions -}.
### Task lists
Example:  
```
- [x] Completed task
- [ ] Incomplete task
	- [ ] Sub-task 1
	- [x] Sub-task 2
	- [ ] Sub-task 3
```
Output:  
- [x] Completed task
- [ ] Incomplete task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3  

### Videos
The valid video extensions are `.mp4`, `.m4v`, `.mov`, `.webm`, and `.ogv`.    
Example:  
```
![](/assets/media/markdown_video.mp4)
<iframe src="https://www.youtube.com/embed/yNcLbrfHj-8" frameborder="0" allowfullscreen class="video"></iframe>
```
![](/assets/media/markdown_video.mp4)
![videos](https://phihungdnv.gitlab.io/assets/media/markdown_video.mp4)
<iframe src="https://www.youtube.com/embed/yNcLbrfHj-8" frameborder="0" allowfullscreen class="video"></iframe>
### Pictures
Example:   
```
![image](/assets/media/git_status.png)
```
![image](/assets/media/git_status.png)
  
![image](https://phihungdnv.gitlab.io/assets/media/git_status.png)

### Colors
Example:
	`#F00`  
	`#F00A`  
	`#FF0000`  
	`#FF0000AA`  
	`RGB(0,255,0)`  
	`RGB(0%,100%,0%)`  
	`RGBA(0,255,0,0.7)`  
	`HSL(540,70%,50%)`  
	`HSLA(540,70%,50%,0.7)`

`#F00`  
`#F00A`  
`#FF0000`  
`#FF0000AA`  
`RGB(0,255,0)`  
`RGB(0%,100%,0%)`  
`RGBA(0,255,0,0.7)`  
`HSL(540,70%,50%)`  
`HSLA(540,70%,50%,0.7)`
