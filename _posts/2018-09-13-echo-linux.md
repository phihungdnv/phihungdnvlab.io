---
layout: post
title: Echo newline in Bash prints literal \n
categories: [linux]
view: true
---
```sh
\t -> tab
\n -> newline
echo -e "ngo phi hung\n\tdevops"
```
output
```
ngo phi hung
	devops
```