---
layout: post
title: Python Fabrix 1x
categories: [python]
view: true
tag: [fabric]
---  
**Fabric is best installed via pip**
```sh
pip install fabric==1.14.1
pip install Paramiko==2.4.2
```
`or`
```sh
pip install 'fabric<2.0'
```
**Check version**
```sh
sauron@shadow:~$ fab --version
Fabric 1.14.1
Paramiko 2.4.2
```
`Document pages`: http://docs.fabfile.org/en/1.14/  

**Fabric's Features and Integration with SSH**
```python
run (fabric.operations.run)
sudo (fabric.operations.sudo)
local (fabric.operations.local)
get (fabric.operations.get)
put (fabric.operations.put)
prompt (fabric.operations.prompt)
reboot (fabric.operations.reboot)
```

> Using as a script without fab shell

**Import Fabric API**
```python
from fabric.api import <functions>
```
example: `from fabric.api import env,run`  

`local` funtion: Use for run shell as local host  
```sh
from fabric.api import local

def prepare_deploy():
    local("./manage.py test my_app")
    local("git add -p && git commit")
    local("git push")
```

`settings` function: Use for `Failure handling`  
```python
from fabric.api import settings

def create_user(username,password):
	echo('--> Creating user: {}'.format(username))
	with settings(warn_only=True):
		run("useradd -m -p {} -s /bin/bash {}".format(password,username)) 
```

`env` funtion: environment such as single host, multihost, username, ssh key ...  
```python
from fabric.api import env

env.host_string='<ip>|<hostname>'
env.user='<user_remote_host>'
env.key_filename='/path/private_key'
env.password='<password>'
# env.sudo_password='<passwd>'
env.keepalive=120
env.forward_agent=True 
# Failed --> env.no_agent=True
env.rcfile='/path/var.conf'
#Fabric will try to load on startup and use to update environment variables.
env.ssh_config_path='/path/.ssh/config'
#Leveraging native SSH config files (proxycomand, jump ...)
#http://docs.fabfile.org/en/1.14/usage/execution.html#ssh-config

```

`run` function:  Run on one or more remote server(s) throught ssh connection
```python
from fabric.api import settings,run,env

env.host_string='192.168.1.100'
env.user='hung.ngo'
env.password='123'
def example():
	run("sudo touch /var/log/$(date| awk '{print $1}').txt")
if __name__=='__main__':
	example()

```
Add user to sudoer  
```sh
root@x:# echo " <username> ALL=NOPASSWD:ALL >> /etc/sudoer"
```
`sudo` function: Same `run`  
```python
from fabric.api import settings,run,env

env.host_string='192.168.1.100'
env.user='hung.ngo'
env.password='123'
def example():
	sudo("touch /var/log/$(date| awk '{print $1}').txt")
if __name__=='__main__':
	example()

```
**Run with multihost**
```python
from fabric.api import run,env,settings,execute,runs_once
import sys
import getpass

env.hosts = sys.argv[1:]
env.user = getpass.getuser()

CRED = '\033[91m'
CYELLOW='\033[93m'
CEND = '\033[0m'


def echo(strings):
    print CYELLOW+strings+CEND


def check_cass():
    echo('--> Get Cassandra status')
    with settings(warn_only=True):
	    run("sudo service cassandra status")

def restart_cass():
	with settings(warn_only=True):
		echo('--> Get PID ')
		pid = run("sudo service cassandra status| grep -o '[[:digit:]]*'")
		
		if pid == '':
			echo('--> PID not found')
			echo('--> Restart Cassandra')
			run("sudo service cassandra restart")
		
		else:
			echo("--> Kill Cassandra process")
			run("sudo kill -9 {}".format(pid))
			
			echo('--> Restart Cassandra')
			run("sudo service cassandra restart")

if  __name__== '__main__':
    execute(check_cass)
    execute(restart_cass)
    execute(check_cass)
```
`Usage:` python payload.py host1 host2 hostN  

```python
from fabric.api import run,env,settings,execute,runs_once,put
import sys
import getpass
import paramiko
paramiko.util.log_to_file("filename.log")

env.host_string = sys.argv[1]
env.user = 'root'

CRED = '\033[91m'
CYELLOW='\033[93m'
CEND = '\033[0m'


def echo(strings):
    print CYELLOW+strings+CEND

def create_user(username,password):
	echo('--> Creating user: {}'.format(username))
	with settings(warn_only=True):
		run("useradd -m -p {} -s /bin/bash {}".format(password,username)) 

def set_sudoer(username):
	echo('--> Setting sudoer for {}'.format(username))
	with settings(warn_only=True):
		run("echo '{} ALL=NOPASSWD:ALL' >> /etc/sudoer".format(username))
 
def transfer_shell(source_file):
	echo('--> Sending shell to {}'.format(sys.argv[1]))
	with settings(warn_only=True):
		put(source_file,'/tmp',use_sudo=True)

def execute_shell():
	echo('--> Installing Firefox')
	with settings(warn_only=True):
		run("bash /tmp/firefox.sh")

	echo('--> Installing Thunderbird')
	with settings(warn_only=True):
		run("bash /tmp/thunderbird.sh")

if  __name__== '__main__':
    username = sys.argv[2]
    password = sys.argv[3]
    dir_with_wildcard = sys.argv[4]
    create_user(username,password)
    set_sudoer(username)
    transfer_shell(dir_with_wildcard)
    #execute_shell()

```	
`Usage:` python payload.py host user pass dir_shell   