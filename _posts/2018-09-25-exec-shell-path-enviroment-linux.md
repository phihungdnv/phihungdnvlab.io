---
layout: post
title:  Exec shell with PATH Enviroment - linux
categories: [linux]
view: true
---
**Create script workspace** 
```sh
$ mkdir -p /home/$USER/src/bin
```
**Create a script in workspace `vi /home/$USER/src/bin/docker_helper`. Example shell script:**
```sh
#!/bin/bash
echo
echo "**************LIST IMAGES***************"
echo
docker images
echo
echo "**************LIST CONTAINER***************"
echo
docker ps -a
```
**Change permission**
```sh
$ sudo chmod 755 /home/$USER/src/bin/docker_helper
```
**Add the folder to the PATH in your `.bashrc`**
```sh
$ echo 'export PATH=$PATH:/home/$USER/src/bin' >> $home/.bashrc
```
**Logout and Login to apply this config**

