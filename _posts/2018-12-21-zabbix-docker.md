---
layout: post
title: Install Zabbix container
categories: [zabbix]
view: true
tag: [zabbix,docker]
---  

**MySQL container**
```sh
docker run --name mysql-server -t \
  -e MYSQL_DATABASE="zabbix" \
  -e MYSQL_USER="zabbix" \
  -e MYSQL_PASSWORD="zabbix" \
  -e MYSQL_ROOT_PASSWORD="root" \
  -d mysql:5.7 \
  --character-set-server=utf8 --collation-server=utf8_bin
```
**Zabbix server**
```sh
docker run --name zabbix-server-mysql -t \
  -e DB_SERVER_HOST="mysql-server" \
  -e MYSQL_DATABASE="zabbix" \
  -e MYSQL_USER="zabbix" \
  -e MYSQL_PASSWORD="zabbix" \
  -e MYSQL_ROOT_PASSWORD="root" \
  --link mysql-server:mysql \
  -p 10051:10051 \
  -d zabbix/zabbix-server-mysql:centos-trunk
```
**Zabbix web interface**
```sh
docker run --name zabbix-web-nginx-mysql -t \
  -e DB_SERVER_HOST="mysql-server" \
  -e MYSQL_DATABASE="zabbix" \
  -e MYSQL_USER="zabbix" \
  -e MYSQL_PASSWORD="zabbix" \
  -e MYSQL_ROOT_PASSWORD="root" \
  -e PHP_TZ="Asia/Riyadh" \
  --link mysql-server:mysql \
  --link zabbix-server-mysql:zabbix-server \
  -p 8000:80 \
  -d zabbix/zabbix-web-nginx-mysql:centos-trunk
```
**Result**
```sh
sauron@shadow:~$ docker ps 
CONTAINER ID        IMAGE                                        COMMAND                  CREATED             STATUS              PORTS                           NAMES
55b3710fcd3f        zabbix/zabbix-web-nginx-mysql:centos-trunk   "docker-entrypoint.sh"   2 days ago          Up 10 hours         443/tcp, 0.0.0.0:8000->80/tcp   zabbix-web-nginx-mysql
4635ab1aae4b        zabbix/zabbix-server-mysql:centos-trunk      "docker-entrypoint.sh"   2 days ago          Up 10 hours         0.0.0.0:10051->10051/tcp        zabbix-server-mysql
cf3503c41d4d        mysql:5.7                                    "docker-entrypoint.s…"   2 days ago          Up 10 hours         3306/tcp, 33060/tcp             mysql-server

```
**Access container**
```sh
docker exec -it <container_id|container_name> /bin/bash

```
**Zabbix logs**
```sh
docker logs <container_id|container_name>
```




