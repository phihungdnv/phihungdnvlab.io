---
layout: post
title:  Fixed docker-runc not installed on system Red Hat Enterprise Linux - DOCKER
categories: [docker]
view: true
tag: docker
---

`sudo ln -s /usr/libexec/docker/docker-runc-current /usr/bin/docker-runc`
