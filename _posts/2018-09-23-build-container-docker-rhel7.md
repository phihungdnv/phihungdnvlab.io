---
layout: post
title: Red Hat base container image Red Hat Enterprise Linux [11] - DOCKER
categories: [docker]
view: true
tag: docker
---
Red Hat develops two types of base images: `standard` and `minimal`.  

>Red Hat strongly recommends that you use RHEL standard or minimal base images as the basis for your containers. Using these images provide the best foundation of secure, tested, and compliant software on which to build your images. While images built from other base images will often run on RHEL systems, Red Hat does not officially support those images

###  STANDARD RHEL BASE IMAGES  
**Features:**
- init system
- yum
- python2.7
- ...

**Find standard base images**
```sh
[rhel7@localhost ~]$ docker search rhel7 | grep redhat.com/rhel7 | grep platform
redhat.com   registry.access.redhat.com/rhel7                                     This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.0                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.1                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.2                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.3                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.4                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7.5                                   This platform image provides a minimal run...   0
redhat.com   registry.access.redhat.com/rhel7/rhel                                This platform image provides a minimal run...   0
```
**Get images from registry**
```sh
$ docker pull registry.access.redhat.com/rhel7/rhel 
```

**Open a shell inside the container**
```
$ docker run -it <imagesID> bash
```

** Check installed packages**
```sh
[root@a89c73720125 /]# rpm -qa | head -5 && rpm -qa | wc -l
tzdata-2018e-3.el7.noarch
setup-2.8.71-9.el7.noarch
basesystem-10.0-7.el7.noarch
nss-softokn-freebl-3.36.0-5.el7_5.x86_64
glibc-2.17-222.el7.x86_64
155
```
**Building container images with standard base images**
Create `Dockerfile`
```
FROM <imageID>
MAINTAINER phihung.ngo <phihungdnv@gmail.com>
# Install httpd, update image and clear cache
RUN yum -y install httpd && yum -y update && yum clean all
# Add some data to web server
RUN echo -e 'Hi phihung\n This is an index page' > /var/www/html/index.html/index
# Port forwarding
EXPOSE 80
ENTRYPOINT ["usr/sbin/httpd"]
CMD ["-D","FORCEBACKGROUND"]
```

Build image  
```sh
$ docker build --rm -t httpd.image - < Dockerfile
```

Run the image  
```sh
$ docker run --name web1 -d -p 8080:80 <imageID> 
```

Check listen port
```sh
[rhel7@localhost ~]$ netstat -ant | egrep '8081|8080'
tcp6       0      0 :::8080                 :::*                    LISTEN
tcp6       0      0 :::8081                 :::*                    LISTEN
```


