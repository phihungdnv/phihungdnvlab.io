---
layout: post
title:  Security Enhanced Linux (SELinux) - RHEL 7
categories: [RedHat7]
view: true
---
>Security Enhanced Linux (SELinux) is an additional layer of system security that determines which process can access which files, directories, and ports.

**SELinux has two possible states**
- Enabled  
- Disabled  

**When SElinux enable, it can run in one of the following modes:
- Enfocing  
- Permissive  

**By default, SELinux operates in permissive mode during the installation and in enforcing mode when the installation has finished**
**Display the current SELinux mode in effect:**
```sh
[rhel7@172 ~]$ getenforce
Enforcing
```
**Switch between the SELinux modes**
```sh
# setenforce Enforcing
# setenforce Permissove
```
**Disabled SELinux, modify the SELINUX variable in the `/etc/selinux/config` configuration file.**
```sh
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of three two values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```
**Reboot server to apply this config**


