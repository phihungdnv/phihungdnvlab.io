---
layout: post
title: gitlab bassic
categories: [git]
fullview: true
---

```
# authorized login issue
	git config --system --unset credential.helper

# git add and commit all
	git add --all && git commit --all -m "massage"

# git rm <remove file or folder>
	git rm <filename>
	git commit -m "message"

# git remove file only from the git resposity, not remove it from the filesystem
	git rm --cached <filename>
	git commit -m "message"

# git push change to remove repo
	git push origin master
```
