---
layout: post
title:  Running docker container Red Hat Enterprise Linux [6] - DOCKER
categories: [docker]
view: true
tag: docker
---
**Basic argument**
`docker run --name {container_name} -p {host_port}:{container_port} -v {/host_path}:{/container_path} -it {image_name} /bin/bash`
**Example 1: Run quick command into container**
```sh
[rhel7@localhost ~]$ docker run --rm fd9364b34cc1 ls /root/buildinfo
Dockerfile-rhel7-7.5-424
Dockerfile-rhel7-rsyslog-7.5-17
```
**Example 2: Run a shell inside the container**
```sh
docker run --name container_rhel7 -it rhel /bin/bash
```
*Options:*
--name:  Set name of container  
-i: creates an interactive session  
-t: open a terminal session

### Running a Docker Container in Detached Mode   

**Example 3: Port Forwarding**
```sh
docker run --name httpd.app -d -it -p 9000:80 968384e7a2cc
```
`-p 9000:80` -> Port 80 forward to port 9000 on localhost  

**Example 4: Mount log files**
```sh
[rhel7@localhost ~]$ cd local-httpd/
[rhel7@localhost local-httpd]$ mkdir http_log
[rhel7@localhost local-httpd]$ ll
total 4
-rw-rw-r--. 1 rhel7 rhel7 172 Sep 19 23:43 Dockerfile
drwxrwxr-x. 2 rhel7 rhel7   6 Sep 20 01:09 http_log
[rhel7@localhost local-httpd]$
```
```sh
[rhel7@localhost local-httpd]$ docker run --name http.app.log -d -it -p 9999:80 -v /home/rhel7/local-httpd/http_log/:/var/log/httpd 968384e7a2cc
8e27c9b0193e836006c1ce3d750e8b1b3011a93673d12331511a519ba9dc5a14
[rhel7@localhost local-httpd]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS                  NAMES
8e27c9b0193e        968384e7a2cc        "/usr/sbin/httpd -..."   13 seconds ago      Exited (1) 12 seconds ago                          http.app.log
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   About an hour ago   Up 27 minutes               0.0.0.0:9000->80/tcp   httpd.app
```