---
layout: post
title:  Managing system services - RHEL 7
categories: [RedHat7]
view: true
---
#### Type1: Using `service` command
```sh			
service <name> start 	->	Starts a service
service <name> stop		->	Stops a service
service <name> restart		-> Restarts a service
service <name> condrestart	-> Restarts a service only if it running
service <name> reload		-> Reloads configuration
service <name> status		-> Check if a service is running
service --status-all		-> Get status of allservices
```
#### Type2: Using `systemctl` command
```sh
systemctl start <name.service> -> Starts a service
systemctl stop <name.service>  -> Stops a service
systemctl restart  <name.service> -> Restarts a service
systemctl try-restart <name.service> -> Restarts a service only if it running
systemctl reload <name.service> -> Reloads configuration
systemctl status <name.service>	-> Check if a service is running
systemctl is-active <name.service> -> Check if a service is running
systemctl list-units --type service --all -> Get status of allservices


```

#### Listing services
**List all curently loaded service units:**
```sh
$ sudo systemctl list-units --type service
```
**List all loaded units regardless of their state**
```sh
$ sudo systemctl list-units --type service --all
```
**List all available service units to see if they are `enabled`, `disabled`, `static`**
```sh
$ sudo systemctl list-unit-files --type service | grep 'enabled'
$ sudo systemctl list-unit-files --type service | grep 'disabled'
$ sudo systemctl list-unit-files --type service | grep 'static'
```

#### Displaying service status
**Only verify that a particular service unit is running**
```sh
$ sudo systemctl is-active <name.service>
```
**Determine whether a particular service unit is enabled**
```sh
$ sudo systemctl is-enabled <name.service>
```
**Displaying Services Ordered to Start Before a Service*
```sh
$ sudo systemctl list-dependencies --after  sshd.service
```
**Displaying Services Ordered to Start After a Service**
```sh
$ sudo systemctl list-dependencies --before  sshd.service
```