---
layout: post
title: Attach docker container Red Hat Enterprise Linux [7] - DOCKER
categories: [docker]
view: true
tag: docker
---

**First_Docker Container in Detached Mode**
```sh
$ docker run --name http.app.v1 -d -p 1234:80 httpd
```
**Attach to container**
```sh
[root@localhost ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                           PORTS                  NAMES
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   2 days ago          Up 59 seconds                    0.0.0.0:9000->80/tcp   httpd.app
```
```sh
[root@localhost ~]# docker exec -it c7e1e24494a6 bash

[root@c7e1e24494a6 /]# netstat -antp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp6       0      0 :::80                   :::*                    LISTEN      1/httpd
```





