---
layout: post
title: Python SCPClient with Paramiko
categories: [python]
view: true
--- 

**transfer_file.py** 
```python
from paramiko import SSHClient
from scp import SCPClient
from configparser import ConfigParser
import sys
conf_path = 'transfer_file.conf'
config = ConfigParser()
config.read(conf_path)

remote_host = config['ENV']['REMOTE_HOST']
username = config['ENV']['USERNAME']
password = config['ENV']['PASSWORD']
sshport = config['ENV']['SSHPORT']
ssh = SSHClient()
ssh.load_system_host_keys()
ssh.connect(hostname=remote_host, port=int(sshport), username=username,password=password)

def progress(filename, size, sent):
    sys.stdout.write("%s\'s progress: %.2f%%   \r" % (filename, float(sent)/float(size)*100) )

def progress4(filename, size, sent, peername):
    sys.stdout.write("(%s:%s) %s\'s progress: %.2f%%   \r" % (peername[0], peername[1], filename, float(sent)/float(size)*100) )

with SCPClient(ssh.get_transport(), progress4=progress4) as scp:
	scp.put('/home/sauron/kali-linux-light-2018.4-amd64.iso','/tmp/')
	#scp.get('/tmp/xxx.log','/home/sauron')
	#Uploading the 'test' directory with its content in the
	#scp.put('/home/sauron/viki', recursive=True, remote_path='/tmp/')
	scp.close()
```
**transfer_file.conf**
```ssh
; transfer_file.conf
[ENV]
REMOTE_HOST = 192.168.10.85
USERNAME = root
PASSWORD = evizi-proxy
SSHPORT = 22
```