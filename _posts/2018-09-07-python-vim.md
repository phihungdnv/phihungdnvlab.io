---
layout: post
title: Setting up Vim for Python development
categories: [python]
view: true
---
```sh
apt-get update
apt-get install vim 
apt-get install curl vim exuberant-ctags git ack-grep
pip install pep8 flake8 pyflakes isort yapf
rm -rf .vimrc
wget https://raw.githubusercontent.com/phihungdnv/fisa-vim-config/master/.vimrc
```
Reference  
http://bit.ly/lp_vim﻿  
https://github.com/fisadev/fisa-vim-config
xxxx
