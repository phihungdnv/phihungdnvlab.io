---
layout: post
title: Getting images from remote Docker registries Red Hat Enterprise Linux [2] - DOCKER
categories: [docker]
view: true
tag: docker
---
`docker pull`: Use the pull option to pull an image from a remote registry.   
```
# docker pull <registry>[:port]/[<namespace>/]<name>:<tag>
```
Namespace examles (<namespace>/<name>)  
organization	redhat/kubernetes, google/kubernetes  
login (username)	phihung/app, phihung.ngo/appv2  
role	devel/database, stag/database, prod/database  
>Docker registry that Red Hat supports at the moment is the one at `registry.access.redhat.com`

Example:
```
# docker pull registry.access.redhat.com/rhel7172.16.20.129/rhel
```
If no tag is provided, Docker Engine uses the :latest tag as a default.  

`docker load`: Load container image from offline compress file  
Example:  
```
# docker load -i rhel-server-docker-x.xxx.tar.gz
```