---
layout: post
title: Top - Unix/Linux
categories: [linux]
view: true
---
```sh
root@pentest:~# top
top - 16:45:11 up 97 days,  9:23,  1 user,  load average: 0.00, 0.00, 0.00
Tasks: 108 total,   1 running, 107 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  3781904 total,   584792 free,   391900 used,  2805212 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2942192 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
    1 root      20   0  221128   9496   6824 S  0.0  0.3   5:31.24 /lib/systemd/systemd --system --deserialize 19
    2 root      20   0       0      0      0 S  0.0  0.0   0:00.12 [kthreadd]
    4 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [kworker/0:0H]
    6 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [mm_percpu_wq]
    7 root      20   0       0      0      0 S  0.0  0.0   0:36.92 [ksoftirqd/0]
    8 root      20   0       0      0      0 S  0.0  0.0   1:47.59 [rcu_sched]
    9 root      20   0       0      0      0 S  0.0  0.0   0:00.00 [rcu_bh]
   10 root      rt   0       0      0      0 S  0.0  0.0   0:00.00 [migration/0]
   11 root      rt   0       0      0      0 S  0.0  0.0   0:26.00 [watchdog/0]
   12 root      20   0       0      0      0 S  0.0  0.0   0:00.00 [cpuhp/0]
   13 root      20   0       0      0      0 S  0.0  0.0   0:00.00 [kdevtmpfs]
   14 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [netns]
   15 root      20   0       0      0      0 S  0.0  0.0   0:04.86 [khungtaskd]
   16 root      20   0       0      0      0 S  0.0  0.0   0:00.00 [oom_reaper]
   17 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [writeback]
   18 root      20   0       0      0      0 S  0.0  0.0   0:00.00 [kcompactd0]
   19 root      25   5       0      0      0 S  0.0  0.0   0:00.00 [ksmd]
   20 root      39  19       0      0      0 S  0.0  0.0   2:33.23 [khugepaged]
   21 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [crypto]
   22 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [kintegrityd]
   23 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [kblockd]
   24 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [ata_sff]
   25 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [md]
   26 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [edac-poller]
   27 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 [devfreq_wq]
```
*Details*  
__The frst line of the display is shown as follows:__      
```sh
top - 16:45:11 up 97 days,  9:23,  1 user,  load average: 0.00, 0.00, 0.00
```
The description of felds in the frst line is as follows:  
- Current time  
- System uptime  
- Number of users logged in  
- Load average of 5, 10, 15 minutes, respectively
  
__The second line is shown as follows:__   
```sh
Tasks: 108 total,   1 running, 107 sleeping,   0 stopped,   0 zombie
```
This line shows the summary of tasks or processes. It shows the total number of all the processes, which includes the total number of running, sleeping, stopped,
and zombie processes.
__The third line is shown as follows:__  
```sh
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
```
This line shows information about CPU usage in % in different modes as follows:  
- us (user): CPU usage in % for running (un-niced) the user processes  
- sy (system): CPU usage in % for running the kernel processes  
- ni (niced): CPU usage in % for running the niced user processes  
- wa (IO wait): CPU usage in % for waiting for the IO completion  
- hi (hardware interrupts): CPU usage in % for serving hardware interrupts  
- si (software interrupts): CPU usage in % for serving software interrupts  
- st (time stolen): CPU usage in % for time stolen for this vm by the hypervisor  

__The fourth line is shown as follows:__  
```sh
KiB Mem :  3781904 total,   584792 free,   391900 used,  2805212 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2942192 avail Mem
```
This line provides information about memory usage. It shows the physical memory that is used, free, available, and used for buffers. The next line shows the swap memory that is available, used, free, and cached.  
__After this line, we see the table of values with the following columns:__
- PID: This is the ID of the process  
- USER: This is the user that is the owner of the process  
- PR: This is the priority of the process  
- NI: This is the "NICE" value of the process  
- VIRT: This is the virtual memory used by the process  
- RES: This is the physical memory used for the process  
- SHR: This is the shared memory of the process  
- S: This indicates the status of the process: S=sleep, R=running, and Z=zombie (S)  
- %CPU: This is the % of CPU used by this process  
- %MEM: This is the % of RAM used by the process  
- TIME+: This is the total time of activity of this process  
- COMMAND: This is the name of the process 


