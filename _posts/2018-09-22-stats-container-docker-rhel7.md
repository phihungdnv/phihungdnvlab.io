---
layout: post
title: Investigate usage statis Ram, CPU, Disk... container Red Hat Enterprise Linux [10] - DOCKER
categories: [docker]
view: true
tag: docker
---
`$ docker stats` -> all container  
`$ docker stats <containerID>` -> show this container  
Example:
```sh
[rhel7@localhost ~]$ docker stats 26e8bc51a79f
CONTAINER           CPU %               MEM USAGE / LIMIT       MEM %               NET I/O             BLOCK I/O           PIDS
26e8bc51a79f        0.01%               2.734 MiB / 3.685 GiB   0.07%               3.2 kB / 5.32 kB    22.1 MB / 0 B       6
```
