---
layout: post
title: Create, Start, Stop/Kill, Remove docker container Red Hat Enterprise Linux [9] - DOCKER
categories: [docker]
view: true
tag: docker
---

**List images**
```sh
[rhel7@localhost ~]$ docker images
REPOSITORY                                        TAG                 IMAGE ID            CREATED             SIZE
httpd                                             1                   968384e7a2cc        2 days ago          944 MB
docker.io/nginx                                   latest              06144b287844        2 weeks ago         109 MB
registry.access.redhat.com/rhscl/nginx-18-rhel7   latest              ab3b239793e1        3 weeks ago         291 MB
registry.access.redhat.com/rhel7/rhel             latest              f5ea21241da8        6 weeks ago         201 MB
```
**Create container**
```sh
[rhel7@localhost ~]$ docker create --name http.app.v3 -p 9988:80 968384e7a2cc
26e8bc51a79fa1439a826cbecba0473e7f6547ae68e9c8387f333a7d89dfdc45
```
**Get containerID**
```sh
[rhel7@localhost ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                       PORTS                  NAMES
26e8bc51a79f        968384e7a2cc        "/usr/sbin/httpd -..."   9 seconds ago       Created                                             http.app.v3
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   2 days ago          Exited (255) 2 minutes ago   0.0.0.0:9000->80/tcp   httpd.app
```
**Start container**
```sh
[rhel7@localhost ~]$ docker start 26e8bc51a79f
26e8bc51a79f
[rhel7@localhost ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                       PORTS                  NAMES
26e8bc51a79f        968384e7a2cc        "/usr/sbin/httpd -..."   27 seconds ago      Up 4 seconds                 0.0.0.0:9988->80/tcp   http.app.v3
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   2 days ago          Exited (255) 3 minutes ago   0.0.0.0:9000->80/tcp   httpd.app
[rhel7@localhost ~]$ curl localhost:9988 | head -5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3985  100  3985    0     0   324k      0 --:--:-- --:--:-- --:--:--  353k
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head>
                <title>Test Page for the Apache HTTP Server on Red Hat Enterprise Linux</title>
[rhel7@localhost ~]$
```
**Stop/Kill container**
```
docker kill (which will send a SIGKILL signal to the container)
docker stop (which will send a SIGTERM and after a grace period will send a SIGKILL). 
```
**Remove container**
```
docker rm -f <containerID>
```