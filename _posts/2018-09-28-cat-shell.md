---
layout: post
title: Inline the contents of a file (cat command)- Shell
categories: [linux]
view: true
tag: cat
---
`script.sh`  
```sh
cat <<END >file
Hello, World.
END
```

Use the `-n`, `--number` flag to print line number before each line    
```sh
$ cat -n file
1 line 1
2 line 2
3
4 line 4
```

To skip empty lines when counting lines, use the `-b`, `--number-nonblank`  
```sh
$ cat -b file
1 line 1
2 line 2

3 line 4
4 line 5
```