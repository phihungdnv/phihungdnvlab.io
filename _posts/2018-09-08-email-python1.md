---
layout: post
title: send email via python script
categories: [python]
view: true
---
**Examle 1**
```python
import smtplib
from getpass import getpass
import random
from email.message import EmailMessage
from email.header import Header
msg = EmailMessage()

##########
gmail_server = 'smtp.gmail.com'
gmail_port = '587'
gmail_user = 'phihungdnv'
gmail_pass = getpass(prompt='email password: ')
########## 
def gmail_smtp(gmail_server,gmail_port,gmail_user,gmail_pass,msg):
	server = smtplib.SMTP(gmail_server,gmail_port)
	server.connect(gmail_server,gmail_port)
	server.starttls()
	server.login(gmail_user,gmail_pass)
	server.send_message(msg)
	print('Email has been sent')
	server.quit()
destaddr = ['phihungngo249@gmail.com','hung.ngo@evizi.com']

msg['Subject']='Send More mail via Python3'
msg['From']='phihungdnv@gmail.com'
msg['To']= ','.join(destaddr)
msg.set_content('Script send email')
gmail_smtp(gmail_server,gmail_port,gmail_user,gmail_pass,msg)
```
**Email with  argument**
```python
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  8 20:33:17 2018

@author: phihungdnv
"""

import argparse
import smtplib
from getpass import getpass
from email.message import EmailMessage

msg = EmailMessage()

def send_gmail(msg,username,password):
    server = smtplib.SMTP('smtp.gmail.com','587')
    server.connect('smtp.gmail.com','587')
    server.starttls()
    server.login(username,password)
    server.send_message(msg)
    print('Email have been sent')
    server.quit()
    
def main():
    msg = EmailMessage()
    parser = argparse.ArgumentParser(description='Script send email via Python')
    parser.add_argument('--from-addr',dest='fromaddr',metavar='abc@gmail.com',help='whole sent mail',required=True)
    parser.add_argument('--to-addr',dest='toaddr',metavar='xyz@gmail.com',help='whole recieve mail, format: email1,email2,emailn',required=True)
    parser.add_argument('--subject',dest='subject',help='subject of email')
    parser.add_argument('--content',dest='content',help='content of email',required=True)
    args = parser.parse_args()
    if args.fromaddr:
        msg['From']= args.fromaddr
    if args.toaddr:
        msg['To']= args.toaddr
    if args.subject: 
        msg['Subject'] = args.subject
    if args.content:
        msg.set_content(args.content)
    email_password = getpass(prompt='Enter email password: ')
    send_gmail(msg,'phihungdnv',email_password)
if __name__ == '__main__':
    main()
```
```sh
root@pentest:~# python3 mailarg.py  --from-addr phihungdnv@gmail.com  --to-addr hung.ngo@evizi.com  --subject 'Hello, practive hardly' --content 'ngo phi hung'
Enter email password:
Email have been sent
root@pentest:~#
```
**Send email from file with argument**
```python
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 00:15:41 2018

@author: phihungdnv
"""

import argparse
import smtplib
from getpass import getpass
from email.message import EmailMessage

msg = EmailMessage()

def mails_from_file(filepath):
    with open (filepath) as f:
        mails = [lines.strip() for lines in f]
    return mails

def send_gmail(msg,username,password):
    server = smtplib.SMTP('smtp.gmail.com','587')
    server.connect('smtp.gmail.com','587')
    server.starttls()
    server.login(username,password)
    server.send_message(msg)
    print('Email have been sent')
    server.quit()
    
def main():
    msg = EmailMessage()
    parser = argparse.ArgumentParser(description='Script send email via Python')
    parser.add_argument('--from-addr',dest='fromaddr',metavar='abc@gmail.com',help='whole sent mail',required=True)
    #Group to address mail
    toaddr = parser.add_argument_group(title='TO EMAIL ADDRESS')
    toaddr.add_argument('--to-addr',dest='toaddr',metavar='xyz@gmail.com',help='whole recieve mail, format: email1,email2,emailn')
    toaddr.add_argument('--to-addr-file',dest='toaddrfile',metavar='PATH/TO/FILE',help='whole recieve mail from file')
    parser.add_argument('--subject',dest='subject',help='subject of email')
    parser.add_argument('--content',dest='content',help='content of email',required=True)
    args = parser.parse_args()
    if args.fromaddr:
        msg['From']= args.fromaddr
    if args.toaddr:
        msg['To']= args.toaddr
    if args.subject: 
        msg['Subject'] = args.subject
    if args.content:
        msg.set_content(args.content)
    if args.toaddrfile:
        msg['To'] = ','.join(mails_from_file(args.toaddrfile))
    email_password = getpass(prompt='Enter email password: ')
    send_gmail(msg,'phihungdnv',email_password)
if __name__ == '__main__':
    main()
```
output
```sh
root@pentest:~# python3 m3.py  --from-addr phihungndv@gmail.com --to-addr-file /root/maillist.txt --subject 'Testing 03' --content 'hello'
Enter email password:
Email have been sent
```
`maillist.txt` content
```
root@pentest:~# cat /root/maillist.txt
phihungdnv@gmail.com
hung.ngo@gmail.com
```
