---
layout: post
title: SSH  Advance PortForwarding / Tunnel / Jump
categories: [linux]
view: true
---
### Topology
```
 +--------+       +----------------+       +-------+       +-----+ 
 |end_user| <---> | firewall/jump1 | <---> | jump2 | <---> | app |
 +--------+       +----------------+       +-------+       +-----+ 
```
### Info Node
```
end_user
	inet1: 192.168.111.9
	port: 22
firewall/jump1
	inet1: 192.168.111.130
	port:22
	inet2: 192.168.112.130
jump2
	inet1: 192.168.112.129
	port: 22
	inet2: 192.168.113.128
app
	inet1: 192.168.113.129
	port: 2222
```
>A proxy is an intermediary that forwards requests from clients to other servers. Performance improvement, load balancing, security or access control are some reasons proxies are used.  

### Classic SSH Jumphost configuration    
/$HOME/.ssh/config (end_user node)
```sh
Host *
	ForwardAgent yes
	AddressFamily inet
	Protocol 2
	ServerAliveInterval 120
	ServerAliveCountMax 720
	IdentityFile ~/.ssh/id_rsa
Host jump1
	HostName 192.168.111.130
	Port 22
Host jump2
	HostName 192.168.112.129
	Port 22
	ProxyCommand ssh -q -W %h:%p jump
```
`-W` = [-W host:port]   
### SSH Jumphost configuration with netcat (nc)
```sh
Host *
	ForwardAgent yes
	AddressFamily inet
	Protocol 2
	ServerAliveInterval 120
	ServerAliveCountMax 720
	IdentityFile ~/.ssh/id_rsa
Host jump1
	HostName 192.168.111.130
	Port 22
Host jump2
	HostName 192.168.112.129
	Port 22
	ProxyCommand ssh -q jump1 nc -w 120ms  %h %p
	#ProxyCommand ssh -q jump1 sudo nc -w 120ms %h %p
```
nc -w => set connect timeout  

### ProxyJump with SSH and SCP (OpenSSH Version > 7.3)
```sh
----------
[root@end_user .ssh]# ssh -J jump1 jump2 -v
----------
Host *
	ForwardAgent yes
	AddressFamily inet
	Protocol 2
	ServerAliveInterval 120
	ServerAliveCountMax 720
	IdentityFile ~/.ssh/id_rsa
Host jump1
	HostName 192.168.111.130
	Port 22
Host jump2
	HostName 192.168.112.129
	Port 22
	ProxyJump jump1
```
**More Gateways Using ProxyJump**
```
Host *
	ForwardAgent yes
	AddressFamily inet
	Protocol 2
	ServerAliveInterval 120
	ServerAliveCountMax 720
	IdentityFile ~/.ssh/id_rsa
Host jump1
	HostName 192.168.111.130
	Port 22
Host jump2
	HostName 192.168.112.129
	Port 22
	ProxyJump jump1
Host app
	HostName 192.168.113.129
	Port 2222
	ProxyJump jump1,jump2
```

### SSH Multiplexing, speed up connect
```
Host *
	ForwardAgent yes
	ForwardX11 no
	AddressFamily inet
	CheckHostIP no
	Protocol 2
	ServerAliveInterval 120
	ServerAliveCountMax 720
	IdentityFile ~/.ssh/id_rsa
	GSSAPIAuthentication yes
	Compression yes
	CompressionLevel 1
	Cipher arcfour128
	ControlPath ~/.ssh/cm-%r@%h:%p
	ControlMaster auto
	ControlPersist yes
Host jump1
	HostName 192.168.111.130
	Port 22
Host jump2
	HostName 192.168.112.129
	Port 22
	ProxyJump jump1
Host app
	HostName 192.168.113.129
	Port 2222
	Proxyjump jump1,jump2
```
```sh
[root@end_user .ssh]# ll
total 20
-rw-------. 1 root root 225 Sep 14 03:53 authorized_keys
srw-------. 1 root root   0 Sep 16 14:46 cm-root@192.168.111.130:22
srw-------. 1 root root   0 Sep 16 14:48 cm-root@192.168.112.129:22
srw-------. 1 root root   0 Sep 16 14:48 cm-root@192.168.113.129:2222
-rw-r--r--. 1 root root 514 Sep 14 06:16 config
-rw-------. 1 root root 887 Sep 14 01:45 id_rsa
-rw-r--r--. 1 root root 227 Sep 14 01:45 id_rsa.pub
-rw-r--r--. 1 root root 538 Sep 14 04:48 known_hosts
```
```
$ ssh -O check host -> check socket connected on host
$ ssh -O stop host -> stop socket
$ ssh -0 exit -> kill all socket
```
### Forces TTY allocation and passes the SSH traffic as though typed
```sh
[root@end_user ~]# ssh -tt 192.168.111.130 -tt ssh 192.168.112.129 -v
```
### SSH Agent
>SSH connection will refer to the agent to get your private key, so you only have to type your passphrase once at the beginning of your session.

```sh
ps x | grep ssh-agent   -> check agent demamon
eval $(ssh-agent) -> start agent
ssh-add -> add private key to agent
ssh-add -L/l -> list agent key
ssh-add -d name-of-key-file -> delete key
ssh-add -D -> delete all key
```
### Tunneling via Local Port Forwarding
Accept TCPForwarding on SSH server   
`/etc/ssh/sshd_config`
```
AllowTcpForwarding yes
Client config
ssh -L 8080:localhost:80 app
Access: http://localhost:8080
```
### Remote Port Forwarding
```
Client access app node through jump2
app node: ssh -R 0.0.0.0:8080:localhost:80 jump2
Acess: http://jump2:8080
```

### SSH client host config option
```
TCPKeepAlive yes -> send TCP messages to keep a connection standing
ServerAliveInterval 30 -> setting tells SSH to ping the server every 30 seconds
ForwardAgent yes -> Agent Forwarding
```
### Count time connection
```sh
[root@end_user .ssh]# time ssh jump1 -t "exit"
Connection to 192.168.111.130 closed.

real    0m20.695s
user    0m0.010s
sys     0m0.010s
```

### Troubleshoot
```
nc ip port 
=> check firewall/jump1

firewall-cmd --state
systemctl status firewalld
=> check status firewall

firewall-cmd --permanent --add-port=2222/tcp
-> add port 2222, protocol TCP open
firewall-cmd --reload
-> apply all config
firewall-cmd --list-ports
-> list all port 

```

### SSH defind    
/etc/ssh/sshd_config  -> for server  
/etc/ssh/sshd_config  -> for client  

