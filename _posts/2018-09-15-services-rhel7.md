---
layout: post
title:  Jobs and services - RHEL 7
categories: [RedHat7]
view: true
---
**List all services**
```sh
[root@end_user ~]# systemctl -at service
  UNIT                                                  LOAD      ACTIVE   SUB     DESCRIPTION
  auditd.service                                        loaded    active   running Security Auditing Service
  cpupower.service                                      loaded    inactive dead    Configure CPU power related settings
  crond.service                                         loaded    active   running Command Scheduler
  dbus.service                                          loaded    active   running D-Bus System Message Bus
● display-manager.service                               not-found inactive dead    display-manager.service
  dm-event.service                                      loaded    inactive dead    Device-mapper event daemon
  dracut-shutdown.service                               loaded    inactive dead    Restore /run/initramfs
  ebtables.service                                      loaded    inactive dead    Ethernet Bridge Filtering tables
  emergency.service                                     loaded    inactive dead    Emergency Shell
● exim.service                                          not-found inactive dead    exim.service
  firewalld.service                                     loaded    inactive dead    firewalld - dynamic firewall daemon
  getty@tty1.service                                    loaded    active   running Getty on tty1
● ip6tables.service                                     not-found inactive dead    ip6tables.service
● ipset.service                                         not-found inactive dead    ipset.service
● iptables.service                                      not-found inactive dead    iptables.service
  irqbalance.service                                    loaded    inactive dead    irqbalance daemon
● kdump.service                                         loaded    failed   failed  Crash recovery kernel arming
  kmod-static-nodes.service                             loaded    active   exited  Create list of required static device nodes for the current kernel
● lvm2-activation.service                               not-found inactive dead    lvm2-activation.service
  lvm2-lvmetad.service                                  loaded    active   running LVM2 metadata daemon
 ... 
```
```sh
[root@end_user ~]# ls /etc/systemd/system/*.service
/usr/lib/systemd/system/arp-ethers.service                      /usr/lib/systemd/system/rhel-autorelabel.service
/usr/lib/systemd/system/auditd.service                          /usr/lib/systemd/system/rhel-configure.service
/usr/lib/systemd/system/autovt@.service                         /usr/lib/systemd/system/rhel-dmesg.service
/usr/lib/systemd/system/blk-availability.service                /usr/lib/systemd/system/rhel-domainname.service
/usr/lib/systemd/system/brandbot.service                        /usr/lib/systemd/system/rhel-import-state.service
/usr/lib/systemd/system/console-getty.service                   /usr/lib/systemd/system/rhel-loadmodules.service
/usr/lib/systemd/system/console-shell.service                   /usr/lib/systemd/system/rhel-readonly.service
/usr/lib/systemd/system/container-getty@.service                /usr/lib/systemd/system/rsyslog.service
/usr/lib/systemd/system/cpupower.service                        /usr/lib/systemd/system/selinux-policy-migrate-local-changes@.service
/usr/lib/systemd/system/crond.service                           /usr/lib/systemd/system/serial-getty@.service
/usr/lib/systemd/system/dbus-org.freedesktop.hostname1.service  /usr/lib/systemd/system/sshd-keygen.service
...
```
**List running services**
```sh
[root@end_user ~]# systemctl -t service --state=active
UNIT                               LOAD   ACTIVE SUB     DESCRIPTION
auditd.service                     loaded active running Security Auditing Service
crond.service                      loaded active running Command Scheduler
dbus.service                       loaded active running D-Bus System Message Bus
getty@tty1.service                 loaded active running Getty on tty1
kmod-static-nodes.service          loaded active exited  Create list of required static device nodes for the current kernel
lvm2-lvmetad.service               loaded active running LVM2 metadata daemon
lvm2-monitor.service               loaded active exited  Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling
lvm2-pvscan@8:2.service            loaded active exited  LVM2 PV scan on device 8:2
network.service                    loaded active exited  LSB: Bring up/down networking
NetworkManager-wait-online.service loaded active exited  Network Manager Wait Online
NetworkManager.service             loaded active running Network Manager
polkit.service                     loaded active running Authorization Manager
postfix.service                    loaded active running Postfix Mail Transport Agent
rhel-dmesg.service                 loaded active exited  Dump dmesg to /var/log/dmesg
rhel-domainname.service            loaded active exited  Read and set NIS domainname from /etc/sysconfig/network
rhel-import-state.service          loaded active exited  Import network configuration from initramfs
rhel-readonly.service              loaded active exited  Configure read-only root support
rsyslog.service                    loaded active running System Logging Service
sshd.service                       loaded active running OpenSSH server daemon
systemd-journal-flush.service      loaded active exited  Flush Journal to Persistent Storage
systemd-journald.service           loaded active running Journal Service
systemd-logind.service             loaded active running Login Service
systemd-random-seed.service        loaded active exited  Load/Save Random Seed
systemd-remount-fs.service         loaded active exited  Remount Root and Kernel File Systems
systemd-sysctl.service             loaded active exited  Apply Kernel Variables
systemd-tmpfiles-setup-dev.service loaded active exited  Create Static Device Nodes in /dev
systemd-tmpfiles-setup.service     loaded active exited  Create Volatile Files and Directories
systemd-udev-trigger.service       loaded active exited  udev Coldplug all Devices
systemd-udevd.service              loaded active running udev Kernel Device Manager
systemd-update-utmp.service        loaded active exited  Update UTMP about System Boot/Shutdown
systemd-user-sessions.service      loaded active exited  Permit User Sessions
systemd-vconsole-setup.service     loaded active exited  Setup Virtual Console
tuned.service                      loaded active running Dynamic System Tuning Daemon

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

33 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
[root@end_user ~]#
```
**Start/stop/Enable/Disable/Status service**
```sh
systemctl start name.service 
systemctl stop name.service
systemctl enable name.service 
systemctl disable name.service
systemctl status name.service
```
