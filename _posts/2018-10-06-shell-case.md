---
layout: post
title:  SHELL - Switch statement with case
categories: [shell]
view: true
---
**Syntax of bash case statement**
```sh
case $variable in
	pattern-1)      
		commands
		;;
	pattern-2)      
		commands
		;;
	pattern-3|pattern-4|pattern-5)
		commands
		;; 
	pattern-N)
		commands
		;;
	*)
		commands
		;;
esac
```
