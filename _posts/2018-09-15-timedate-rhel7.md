---
layout: post
title:  Configure time and date -RHEL 7
categories: [RedHat7]
view: true
---
```sh
[root@end_user ~]# timedatectl
      Local time: Mon 2018-09-17 04:29:17 +07
  Universal time: Sun 2018-09-16 21:29:17 UTC
        RTC time: Sun 2018-09-16 21:29:45
       Time zone: Asia/Ho_Chi_Minh (+07, +0700)
     NTP enabled: n/a
NTP synchronized: no
 RTC in local TZ: no
      DST active: n/a
```
```sh
[root@end_user ~]# date
Mon Sep 17 04:29:36 +07 2018
```
**Display the time in UTC**
```sh
$ date --utc
```
**Customize the format of the displayed information**
```
$date +"format"
```
```
%F -> YYYY-MM-DD format (for example, 2016-09-16)
%T -> HH:MM:SS format (for example, 17:30:24)
%Y -> YYYY format (for example, 2016)
%m -> MM format (for example, 09)
%d -> The day of the month in the DD format (for example, 16)
%S -> The second in the SS format (for example, 24).
%M -> The minute in the MM format (for example, 30)
%H -> The hour in the HH format (for example, 17)
```

```sh
[root@end_user ~]# cal
   September 2018
Su Mo Tu We Th Fr Sa
                   1
 2  3  4  5  6  7  8
 9 10 11 12 13 14 15
16 17 18 19 20 21 22
23 24 25 26 27 28 29
30
```
```sh
[root@end_user ~]# cal -y
                               2018

       January               February                 March
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6                1  2  3                1  2  3
 7  8  9 10 11 12 13    4  5  6  7  8  9 10    4  5  6  7  8  9 10
14 15 16 17 18 19 20   11 12 13 14 15 16 17   11 12 13 14 15 16 17
21 22 23 24 25 26 27   18 19 20 21 22 23 24   18 19 20 21 22 23 24
28 29 30 31            25 26 27 28            25 26 27 28 29 30 31

        April                   May                   June
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
 1  2  3  4  5  6  7          1  2  3  4  5                   1  2
 8  9 10 11 12 13 14    6  7  8  9 10 11 12    3  4  5  6  7  8  9
15 16 17 18 19 20 21   13 14 15 16 17 18 19   10 11 12 13 14 15 16
22 23 24 25 26 27 28   20 21 22 23 24 25 26   17 18 19 20 21 22 23
29 30                  27 28 29 30 31         24 25 26 27 28 29 30

        July                  August                September
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
 1  2  3  4  5  6  7             1  2  3  4                      1
 8  9 10 11 12 13 14    5  6  7  8  9 10 11    2  3  4  5  6  7  8
15 16 17 18 19 20 21   12 13 14 15 16 17 18    9 10 11 12 13 14 15
22 23 24 25 26 27 28   19 20 21 22 23 24 25   16 17 18 19 20 21 22
29 30 31               26 27 28 29 30 31      23 24 25 26 27 28 29
                                              30
       October               November               December
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6                1  2  3                      1
 7  8  9 10 11 12 13    4  5  6  7  8  9 10    2  3  4  5  6  7  8
14 15 16 17 18 19 20   11 12 13 14 15 16 17    9 10 11 12 13 14 15
21 22 23 24 25 26 27   18 19 20 21 22 23 24   16 17 18 19 20 21 22
28 29 30 31            25 26 27 28 29 30      23 24 25 26 27 28 29
                                              30 31
```
**Synchronize time and date**
```
# yum  -y install ntp

Get the list of all the available time zones
# timedatectl list-timezones

Set time zone
# timedatectl set-timezone America/Los_Angeles

systemctl enable ntpd && systemctl start ntpd
```

**To change the current time, type the following at a shell prompt as root:**
```sh
# timedatectl set-time HH:MM:SS
# date --set HH:MM:SS
```
**To change the current date, type the following at a shell prompt as root:**
```sh
# timedatectl set-time YYYY-MM-DD
# date --set YYYY-MM-DD
```
**Change date and time**
```sh
# timedatectl set-time 2017-06-02 23:26:00
# date --set "2017-06-02 23:26:00"
```




