---
layout: post
title:  Sysemd unit files  - RHEL 7
categories: [RedHat7]
view: true
tag: [systemctl, sudo]
---
#### Systemd Unit Files Locations
```sh
/usr/lib/systemd/system/ -> Systemd unit files distributed with installed RPM packages.

/run/systemd/system/ -> Systemd unit files created at run time. This directory takes precedence over the directory with installed service unit files.

/etc/systemd/system/  -> Systemd unit files created by systemctl enable as well as unit files added for extending a service. This directory takes precedence over the directory with runtime unit files.
```
>The `/etc/systemd/system/` directory is reserved for unit files created or customized by the system administrator.

Unit file names take the following form: `unit_name.type_extension`  
Unit files can be supplemented with a directory for additional configuration files. For example, to add custom configuration options to `sshd.service`, create the `sshd.service.d/custom.conf` file and insert additional directives there  


