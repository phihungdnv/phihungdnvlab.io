---
layout: post
title:  SHELL - For loop to list iterate over numbers
categories: [shell]
view: true
---

```sh
#! /bin/bash 
for i in {1..10}; do
	echo $i
done
```
This outputs the following:
```
1
2
...
...
10
```
