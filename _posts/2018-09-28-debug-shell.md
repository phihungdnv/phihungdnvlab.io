---
layout: post
title:  Debug executed - Shell
categories: [shell]
view: true
---
**The -x argument enables you to walk through each line in the script. One good example is here:**
```sh
#!/usr/bin/env bash
echo "Display all IP"
ip=`ip -4 a | grep inet | awk '{print $2}' | cut -d / -f 1`
echo -e  "$ip"
```
**Executed script without debug mode**
```sh
[rhel7@vhost1 ~]$ bash getip.sh
Display all IP
127.0.0.1
172.16.20.132
192.168.122.1
172.17.0.1
```

**Debug mode**
```sh
[rhel7@vhost1 ~]$ bash -x getip.sh
+ echo 'Display all IP'
Display all IP
++ ip -4 a
++ cut -d / -f 1
++ awk '{print $2}'
++ grep inet
+ ip='127.0.0.1
172.16.20.132
192.168.122.1
172.17.0.1'
+ echo -e '127.0.0.1
172.16.20.132
192.168.122.1
172.17.0.1'
127.0.0.1
172.16.20.132
192.168.122.1
172.17.0.1
```