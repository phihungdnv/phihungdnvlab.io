---
layout: post
title:  Web Server, Virtual Host (httpd) - RHEL 7
categories: [RedHat7]
view: true
tag: httpd
---
**Install httpd service**
```sh
$ sudo yum -y install httpd
```
**Description Configuration Files**
```
/etc/httpd/conf/httpd.conf -> The main configuration file
/etc/httpd/conf.d/ -> An auxiliary directory fo configuration files that included in the main configuration file 
/etc/httpd/conf.d/autoindex.conf -> This configures mod_autoindex directory indexing.
/etc/httpd/conf.d/userdir.conf -> This configures access to user directories, for example, http://example.com/~username/; such access is disabled by default for security reasons.
/etc/httpd/conf.d/welcome.conf —> As in previous releases, this configures the welcome page displayed for http://localhost/ when no content is present.
```
**To check the configuration for possible errors, type the following at a shell prompt:**
```sh
$ sudo apachectl configtest
```
**Enabled and Start httpd.service**
```sh
$ sudo systemctl enable httpd.service
$ sudo systemctl start httpd.service
```
**Firewall open port 80**
```sh
$ sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
$ sudo firewall-cmd --reload
```
### Virtual Host
**Create DocumentRoot directories**
```sh
$ sudo mkdir /var/www/vhost1.local
$ sudo mkdir /var/www/vhost2.local
```
**Create `index.html` in `/var/www/vhost<1|2>.local/public_html`**
```sh
$ sudo mkdir -p /var/www/vhost1.local/public_html/ && echo "HostName: vhost1.local" | sudo tee --append /var/www/vhost1.local/public_html/index.html

$ sudo mkdir -p /var/www/vhost2.local/public_html/ && sudo sh -c "echo 'HostName: vhost2.local' > /var/www/vhost2.local/public_html/index.html"
```
**Create directories that virtual host store**
```sh
$ sudo mkdir /etc/httpd/sites-available
$ sudo mkdir /etc/httpd/sites-enable
```
`sites-available` -> directory will keep all of our virtual host files  
`sites-enable` -> directory will hold symbolic links to virtual hosts that we want to publish

**Include virtualhost config to main config file**
```sh
$ sudo sh -c "echo 'IncludeOptional sites-enable/*.conf' >> /etc/httpd/conf/httpd.conf"
```
**Create the First Virtual Host File**
`/etc/httpd/sites-available/vhost1.local.conf`
```sh
<VirtualHost *:80>
DocumentRoot "/var/www/vhost1.local/public_html"
ServerName vhost1.local
ServerAlias www.vhost1.local
ErrorLog "/var/log/httpd/vhost1.local-error_log"
CustomLog "/var/log/httpd/vhost1.local-access_log" common
</VirtualHost>
```
`/etc/httpd/sites-available/vhost2.local.conf`
```sh
<VirtualHost *:80>
DocumentRoot "/var/www/vhost2.local/public_html"
ServerName vhost2.local
ServerAlias www.vhost2.local
ErrorLog "/var/log/httpd/vhost2.local-error_log"
CustomLog "/var/log/httpd/vhost2.local-access_log" common
</VirtualHost>
```
**Enable the New Virtual Host Files**
```sh
$ sudo ln -s /etc/httpd/sites-available/vhost1.local.conf /etc/httpd/sites-enable/vhost1.local.conf
$ sudo ln -s /etc/httpd/sites-available/vhost2.local.conf /etc/httpd/sites-enable/vhost2.local.conf
```
**Check config**
```sh
[root@172 conf]# apachectl configtest
Syntax OK
```
**Restart http.service**
```sh
$ systemctl restart httpd
```
**Check the virtual host(s) configuration**
```sh
[root@172 conf]# httpd -D DUMP_VHOSTS
VirtualHost configuration:
*:80                   is a NameVirtualHost
         default server vhost1.local (/etc/httpd/sites-enable/vhost1.local.conf:1)
         port 80 namevhost vhost1.local (/etc/httpd/sites-enable/vhost1.local.conf:1)
                 alias www.vhost1.local
         port 80 namevhost vhost2.local (/etc/httpd/sites-enable/vhost2.local.conf:1)
                 alias www.vhost2.local
```
**Test with curl**
```sh
$ echo -e "172.16.20.132\tvhost1.local\n172.16.20.132\tvhost2.local" | sudo tee --append /etc/hosts
172.16.20.132   vhost1.local
172.16.20.132   vhost2.local
```
```sh
[root@172 conf]# curl http://vhost1.local/
HostName: vhost1.local
[root@172 conf]# curl http://vhost2.local/
HostName: vhost2.local
```
