---
layout: post
title:  SHELL - For, While, Until Loop
categories: [shell]
view: true
---
**Looping over an array**
```sh
#!/usr/bin/env bash
arr=(a b c d 1 2 3)
for i in "${arr[@]}"; do
	echo $i
done
```
`Script executed`
```sh
$ bash foorarr.sh
a
b
c
d
1
2
3
```
**Type2**
```sh
#!/usr/bin/env bash
arr=( a b c d)
for ((i=0; i<${#arr[@]}; i++ )); do
	echo ${arr[$i]}
done	
```
`Script executed`
```sh
[hung.ngo@jumpserver ~]$ bash for2.sh
a
b
c
d
```
**Count array**
```sh
arr=( 1 2 3 4 5 a b c d)
${#arr[@]}
```
**`While` loop**
```sh
#!/usr/bin/env bash
i=0
while [ $i -lt 5 ]
do
	echo $i
	i=$[$i+1]
done
```
**`until` loop**
```sh
i=5
until [ $i -eq 10 ]; do
	echo $i
	i=$((i+1))
done
```