---
layout: post
title: Deploy WordPress with MySQL by using dual images - Rhel7 [13] - DOCKER
categories: [docker]
view: true
tag: docker
---

**Get `mysql` image**
```sh
$ docker pull mysql
```
**Get `wordpress` images**
```sh
$ docker pull wordpress
```
**Build `mysql_container`**
```sh
$ docker run --name mysqlwp -e MYSQL_ROOT_PASSWORD=wordpressdocker \
-e MYSQL_DATABASE=wp_database \
-e MYSQL_USER=phihung \
-e MYSQL_PASSWORD=wordpresspwd \
-d mysql	
```

**Build `wordpress_container`**
```sh
$ docker run --name wordpress --link mysqlwp:mysql -p 80:80 \
-e WORDPRESS_DB_NAME=wp_database \
-e WORDPRESS_DB_USER=phihung \
-e WORDPRESS_DB_PASSWORD=wordpresspwd \
-d wordpress	
```

**Note that Firewall allow as port 3306/tcp**
```sh
# firewall-cmd --list-ports
3306/tcp
```
**Using brower access address: `http://localhost:80` to config wordpress next step**
