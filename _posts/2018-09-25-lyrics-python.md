---
layout: post
title: Lyrics base64 python script
categories: [python]
view: true
---
```python
##!/usr/bin/env python3
## src http://bit.do/extS3
import base64
import json
print ('')
print ('')
lyrics="TmdheSBzZSBraGFjLCBzZSBsYWkgdGhheSBoYW5nIGNheSByYXQgeGFuaA==\nU2UgbGFpIHRoYXkgbmdvbiBnaW8gcmF0IHRyb25nIGxhbmg=\nS2hlIGx1b3QgcXVhIHRpbSBtaW5o\nTmdheSBtYWkgc2Uga2hhYywgc2UgbGFpIHRoYXkgZG9uZyBuZ3VvaSByYXQgZG9uZw==\nU2UgbGFpIHRoYXkgdHJvaSB4YW5oIHJhdCByb25n\nTmFuZyBraGUgc29pIHRyZW4gZGF1\nLi4u\nVmEga2hpIGFuaCBob2FuZyBob24gY2hvdCB0YXQsIGJvbmcgZGVtIGRhbiB4dW9uZyBub2kgZGF5\nVG9pIHNlIGtob25nIGxhIHRvaSBuaHUgbmdheSBob20gbmF5\nLi4u\nQ2hpIGhvbSBuYXkgdGhvaSwgeGluIGNobyB0b2kgZHVvYyBidW9uIGNodXQgdGhvaQ==\nS2hpIG1vdCBhaSBibyBkaSBtYXQgcm9p\nS2hpIG5vaSBuaG8gbmdhcCB0cmFuIGJvbmcgZGVtIGhheSB0cm9uZyBsb25nIHRvaQ==\nQ2hpIGhvbSBuYXkgdGhvaSwgY2hvIG51b2MgbWF0IG5heSBkdW9jIGN1IHJvaQ==\nQ2hvIGNvbiB0aW0gbmF5IGN1IHJhIHJvaQ==\nUXVhIGRlbSBuYXkgcm9pIHNlIGtoYWMgdGhvaQ==\nLi4u\nTmdheSBtYWkgc2Uga2hhYywgc2UgbGFpIHRoYXkgdHVuZyBuaGlwIGJ1b2MgY2hhbg==\nQmFuZyB0cmVuIGNvbiBkdW9uZyBraWEgcmF0IHF1ZW4gdGh1b2M=\nS2kgdWMgeHVhIGtlbyB2ZQ==\nVHJlbiB0dW5nIGdvYyBwaG8sIG5odW5nIGhhbmcgY2F5IG1hIHRhIGRhIHF1YQ==\nTmh1bmcgY2hpZXUgbXVhIG5nb2kgZHVvaSBtYWkgaGllbiBuaGE=\nU2UgbWFpIGtob25nIHF1ZW4gZHVvYw==\n"
arr_lyrics = lyrics.splitlines()
for i in range(len(arr_lyrics)):
        json_encoded_list = json.dumps(arr_lyrics[i])
        decoded_lyrics = base64.b64decode(json_encoded_list)
        print (decoded_lyrics)

print ('')
print ('Ngay mai se khac :)')
print ('Go ahead .................................................................')
"""

"""
```
**bass64 encode**
```
TmdheSBzZSBraGFjLCBzZSBsYWkgdGhheSBoYW5nIGNheSByYXQgeGFuaA==
U2UgbGFpIHRoYXkgbmdvbiBnaW8gcmF0IHRyb25nIGxhbmg=
S2hlIGx1b3QgcXVhIHRpbSBtaW5o
TmdheSBtYWkgc2Uga2hhYywgc2UgbGFpIHRoYXkgZG9uZyBuZ3VvaSByYXQgZG9uZw==
U2UgbGFpIHRoYXkgdHJvaSB4YW5oIHJhdCByb25n
TmFuZyBraGUgc29pIHRyZW4gZGF1
Li4u
VmEga2hpIGFuaCBob2FuZyBob24gY2hvdCB0YXQsIGJvbmcgZGVtIGRhbiB4dW9uZyBub2kgZGF5
VG9pIHNlIGtob25nIGxhIHRvaSBuaHUgbmdheSBob20gbmF5
Li4u
Q2hpIGhvbSBuYXkgdGhvaSwgeGluIGNobyB0b2kgZHVvYyBidW9uIGNodXQgdGhvaQ==
S2hpIG1vdCBhaSBibyBkaSBtYXQgcm9p
S2hpIG5vaSBuaG8gbmdhcCB0cmFuIGJvbmcgZGVtIGhheSB0cm9uZyBsb25nIHRvaQ==
Q2hpIGhvbSBuYXkgdGhvaSwgY2hvIG51b2MgbWF0IG5heSBkdW9jIGN1IHJvaQ==
Q2hvIGNvbiB0aW0gbmF5IGN1IHJhIHJvaQ==
UXVhIGRlbSBuYXkgcm9pIHNlIGtoYWMgdGhvaQ==
Li4u
TmdheSBtYWkgc2Uga2hhYywgc2UgbGFpIHRoYXkgdHVuZyBuaGlwIGJ1b2MgY2hhbg==
QmFuZyB0cmVuIGNvbiBkdW9uZyBraWEgcmF0IHF1ZW4gdGh1b2M=
S2kgdWMgeHVhIGtlbyB2ZQ==
VHJlbiB0dW5nIGdvYyBwaG8sIG5odW5nIGhhbmcgY2F5IG1hIHRhIGRhIHF1YQ==
Tmh1bmcgY2hpZXUgbXVhIG5nb2kgZHVvaSBtYWkgaGllbiBuaGE=
U2UgbWFpIGtob25nIHF1ZW4gZHVvYw==
```