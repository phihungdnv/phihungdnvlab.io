---
layout: post
title: Aliasing - Linux
categories: [linux]
view: true
tag: alias
---
>Shell aliases are a simple way to create new commands or to wrap existing commands with code of your own. They somewhat overlap with shell functions, which are however more versatile and should therefore often be preferred

**Example alias:**
```sh
$ sudo alias ls='ls --colort=auto'
```
**Create an Alias**
```sh
$ sudo alias word='command'
```
**Remove an alias**
```sh
$ sudo unalias <alias_name>
```
**List all aliases**
```sh
$ alias -p
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias vi='vim'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
```

