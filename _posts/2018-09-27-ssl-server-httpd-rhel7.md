---
layout: post
title:  Setting Up an SSL Server with mod_nss (httpd) - RHEL 7
categories: [RedHat7]
view: true
tag: httpd
---
**Enabling the mod_ssl Module**
```sh
$ sudo yum -y install mod_nss
```
This will create the mod_nss configuration file at `/etc/httpd/conf.d/nss.conf`
**Config example in `/etc/httpd/conf.d/nss.conf`**
```sh
...
Listen 443
...
<VirtualHost _default_:443>
```
Mozilla NSS stores certificates in a server certificate database indicated by the `NSSCertificateDatabase` directive in the `/etc/httpd/conf.d/nss.conf file`. By default the path is set to `/etc/httpd/alias`, the NSS database created during installation.  
**To view the default NSS database, issue a command as follows:**
```sh
[root@172 conf]# certutil -L -d /etc/httpd/alias

Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI
cacert                                                       CTu,Cu,Cu
beta                                                         u,pu,u
alpha                                                        u,pu,u
Server-Cert                                                  u,u,u
[root@172 conf]#
```
**To configure mod_nss to use another database, edit the NSSCertificateDatabase line in the `/etc/httpd/conf.d/nss.conf` file**
```sh
NSSCertificateDatabase /etc/httpd/alias
```
**To apply a password to the default NSS certificate database, use the following command as `root`:**
```sh
[root@172 conf]# certutil -W -d /etc/httpd/alias
Enter Password or Pin for "NSS Certificate DB":  <first config password is null, press Enter>
Enter a password which will be used to encrypt your keys.
The password should be at least 8 characters long,
and should contain at least one non-alphabetic character.

Enter new password:
Re-enter password:
Password changed successfully.
```
**Before deploying the HTTPS server, create a new certificate database using a certificate signed by a certificate authority (CA).**
```sh
$ cd ~
$ sudo openssl genrsa -des3 -out myCA.key 2048
$ sudo openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem
$ sudo mkdir /etc/httpd/nss-db-directory/
$ sudo cp myCA.pem certificate.pem
```
```sh
$ sudo certutil -d /etc/httpd/nss-db-directory/ -A -n "CA_certificate" -t CT,, -a -i certificate.pem
```
**Setting a Password for a Mozilla NSS database**
```sh
[root@172 ~]# certutil -W -d /etc/httpd/nss-db-directory/
Enter Password or Pin for "NSS Certificate DB":
Invalid password.  Try again.
Enter Password or Pin for "NSS Certificate DB":  <press enter>
Enter a password which will be used to encrypt your keys.
The password should be at least 8 characters long,
and should contain at least one non-alphabetic character.

Enter new password:
Re-enter password:
Password changed successfully.
```
**Configure mod_nss to use the NSS internal software token by changing the line with the `NSSPassPhraseDialog` directive as follows:**
```sh
NSSPassPhraseDialog  file:/etc/httpd/password.conf
```
**Create the `/etc/httpd/password.conf` file as root:**
```sh
$ echo "internal:123456" | sudo tee --append /etc/httpd/password.conf
```
`123456` -> NSS security databases password
**Apply the appropriate ownership and permissions to the /etc/httpd/password.conf file:**
```sh
$ sudo chgrp apache /etc/httpd/password.conf
$ sudo chmod 640 /etc/httpd/password.conf
```

**To disable all SSL and TLS protocol versions except TLS version 1 and higher, proceed as follows:**
`/etc/httpd/conf.d/nss.conf`
```sh
NSSProtocol TLSv1.0,TLSv1.1
```
**Restart Services**
```sh
$ sudo systemctl restart httpd
```
**Firewall config**
```sh
$ sudo firewall-cmd --permanent --zone=public --add-port=443/tcp
$ sudo firewall-cmd --reload
```

