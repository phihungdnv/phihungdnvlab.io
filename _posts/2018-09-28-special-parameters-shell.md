---
layout: post
title: Special parameters - Shell
categories: [shell]
view: true
---

**Bash environment**
`$#` -> Expands to the number of positional parameters in decimal
`$$` -> Expands to the process ID of the shell
`$0` -> Expands to the name of the shell or shell script

**Example1:**
```sh
#!/usr/bin/env bash
deploy=false
uglify=false

while (($# > 1)); do case $1 in
	--deploy)
		deploy="$2";;
	--uglify)
		uglify="$2";;
	*) break;
	esac; shift 2
done

$deploy && echo "will deploy... deploy = $deploy"
$uglify && echo "will uglify... uglify = $uglify"
```
`executed code`  
```sh
[rhel7@vhost1 ~]$ bash  arg.sh --deploy true --uglify true                                                  
will deploy... deploy = true                                                                                ```
will uglify... uglify = true 
                                                                               
[rhel7@vhost1 ~]$ bash  arg.sh --deploy true --uglify false                                                 
will deploy... deploy = true 
                                                                               
[rhel7@vhost1 ~]$ bash -x  arg.sh --deploy true --uglify false                                              
+ deploy=false                                                                                              
+ uglify=false                                                                                              
+ (( 4 > 1 ))                                                                                               
+ case $1 in                                                                                                
+ deploy=true                                                                                               
+ shift 2                                                                                                   
+ (( 2 > 1 ))                                                                                               
+ case $1 in                                                                                                
+ uglify=false                                                                                              
+ shift 2                                                                                                   
+ (( 0 > 1 ))                                                                                               
+ true                                                                                                      
+ echo 'will deploy... deploy = true'                                                                       
will deploy... deploy = true                                                                                
+ false
 
[rhel7@vhost1 ~]$ bash -x  arg.sh --deploy true --uglify 23
+ deploy=false
+ uglify=false
+ (( 4 > 1 ))
+ case $1 in
+ deploy=true
+ shift 2
+ (( 2 > 1 ))
+ case $1 in
+ uglify=23
+ shift 2
+ (( 0 > 1 ))
+ true
+ echo 'will deploy... deploy = true'
will deploy... deploy = true
+ 23
arg.sh: line 15: 23: command not found  
```

**Example2**
```sh
#!/usr/bin/env bash
deploy=false
uglify=false

while (($# > 1)); do case $1 in
	--deploy)
			deploy=$2;;
	--uglify)
			uglify=$2;;
	*) break;
	esac; shift 2
done

echo "will deploy... deploy = $deploy"
echo "will uglify... uglify = $uglify"
```   
`executed`   
```sh
[rhel7@vhost1 ~]$ bash -x  arg.sh --deploy true
+ deploy=false
+ uglify=false
+ (( 2 > 1 ))
+ case $1 in
+ deploy=true
+ shift 2
+ (( 0 > 1 ))
+ echo 'will deploy... deploy = true'
will deploy... deploy = true
+ echo 'will uglify... uglify = false'
will uglify... uglify = false
[rhel7@vhost1 ~]$ bash arg.sh --deploy true
will deploy... deploy = true
will uglify... uglify = false
[rhel7@vhost1 ~]$ bash arg.sh --deploy 10
will deploy... deploy = 10
will uglify... uglify = false
[rhel7@vhost1 ~]$ bash -x  arg.sh --uglify 23
+ deploy=false
+ uglify=false
+ (( 2 > 1 ))
+ case $1 in
+ uglify=23
+ shift 2
+ (( 0 > 1 ))
+ echo 'will deploy... deploy = false'
will deploy... deploy = false
+ echo 'will uglify... uglify = 23'
will uglify... uglify = 23
```                                                                                              