---
layout: post
title: Delete docker container Red Hat Enterprise Linux [8] - DOCKER
categories: [docker]
view: true
tag: docker
---
**List all state container without container c7e1e24494a6**
```sh
[root@localhost ~]# docker ps -a | grep -v c7e1e24494a6
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                           PORTS                  NAMES
424a91a3ca71        968384e7a2cc        "/usr/sbin/httpd -..."   42 minutes ago      Created                                                 http.app.v5
d664496c69b2        968384e7a2cc        "/usr/sbin/httpd -..."   46 minutes ago      Created                                                 http.app.v4
4545ff3c4cf2        968384e7a2cc        "/usr/sbin/httpd -..."   46 minutes ago      Created                                                 http.app.v3
51a1269a4e7e        968384e7a2cc        "/usr/sbin/httpd -..."   48 minutes ago      Created                                                 http.app.v2
cc6fe17c14ba        f5ea21241da8        "bash"                   About an hour ago   Exited (0) About an hour ago                            admiring_kilby
792d90272d9c        f5ea21241da         "bash"                   About an hour ago   Exited (127) About an hour ago                          objective_lichterman
79b6c83065d0        f5ea21241da8        "bash"                   About an hour ago   Exited (127) About an hour ago                          objective_lovelace
00dc0f58d4f6        f5ea21241da8        "bash"                   About an hour ago   Created                                                 trusting_volhard
820d6b69db6d        f5ea21241da8        "bash"                   About an hour ago   Created                                                 loving_jennings
c9ef75dd4bf0        968384e7a2cc        "/usr/sbin/httpd -..."   11 hours ago        Exited (1) About an hour ago                            log.app
9045e880eb48        06144b287844        "bash"                   2 days ago          Exited (0) 2 days ago                                   nginx.app
d59c03606640        f5ea21241da8        "bash"                   2 days ago          Exited (1) 2 days ago                                   reverent_goldstine
```
**Delete all container**
```sh
[root@localhost ~]# docker rm -f $(docker ps -a | grep -v c7e1e24494a6 | awk '{print $1}' | tail -n +2)                                  
424a91a3ca71                                                                                                                             ```
d664496c69b2                                                                                                                             
4545ff3c4cf2                                                                                                                             
51a1269a4e7e                                                                                                                             
cc6fe17c14ba                                                                                                                             
792d90272d9c                                                                                                                             
79b6c83065d0                                                                                                                             
00dc0f58d4f6                                                                                                                             
820d6b69db6d                                                                                                                             
c9ef75dd4bf0                                                                                                                             
9045e880eb48                                                                                                                             
d59c03606640                                                                                                                             
[root@localhost ~]# docker ps -a                                                                                                         
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES    
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   2 days ago          Up 7 minutes        0.0.0.0:9000->80/tcp   httpd.app
```