---
layout: post
title: Investigate a container image Red Hat Enterprise Linux [5] - DOCKER
categories: [docker]
view: true
tag: docker
---
Reasons for investigating an image beforce you run it include:  
- Understanding what the image does  
- Checking that the image has the latest security patches  
- Seeing if the image opens any special privileges to the host system  

`docker inspect image_[name|id] `: Get some basic information about what an images does, command is executed when run the container images    
Example:  
```sh
[rhel7@localhost ~]$ docker inspect f5ea21241da8
[
    {
        "Id": "sha256:f5ea21241da8d3bc1e92d08ca4888c2f91ed65280c66acdefbb6d2dba6cd0b29",
        "RepoTags": [
            "registry.access.redhat.com/rhel7/rhel:latest"
        ],
        "RepoDigests": [
            "registry.access.redhat.com/rhel7/rhel@sha256:9d3a75b364209190f372645855801d007e7b4f5a4fbff0da83dc64879a808c90"
        ],
        "Parent": "",
        "Comment": "",
        "Created": "2018-08-09T19:04:38.711341Z",
...
```