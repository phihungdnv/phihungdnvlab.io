---
layout: post
title:  Input - Shell
categories: [shell]
view: true
---
```sh
#!/usr/bin/env bash
echo "What's your name?"
read name
echo "Your name is $name"
```
**Exec script**
```sh
$ bash input.sh
What's your name?
phi hung
Your name is phi hung
```
**Append something to the variable value while printing it**
```sh
#!/usr/bin/env bash
echo "What are you doing?"
read action
echo "I am ${action}ing"
```
**Output**
```sh
$ bash input2.sh
What are you doing?
Sleep
I am Sleeping.
```
  