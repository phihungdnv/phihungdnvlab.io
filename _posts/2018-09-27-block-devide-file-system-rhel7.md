---
layout: post
title:  Viewing block devices and file systems - RHEL 7
categories: [RedHat7]
view: true
tag: [lsblk, findmnt]
---
>The lsblk command allows you to display a list of available block devices. It provides more information and better control on output formatting than the blkid command

```sh
$ lsblk
NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0                      11:0    1 1024M  0 rom
sda                       8:0    0   50G  0 disk
├─sda1                    8:1    0  500M  0 part /boot
└─sda2                    8:2    0 49.5G  0 part
  ├─os-root (dm-0)      253:0    0    8G  0 lvm  /
  ├─os-swap (dm-1)      253:1    0    4G  0 lvm  [SWAP]
  ├─os-tmp (dm-3)       253:3    0    2G  0 lvm  /tmp
  ├─os-usr (dm-4)       253:4    0   10G  0 lvm  /usr
  ├─os-var (dm-5)       253:5    0 10.5G  0 lvm  /var
  ├─os-home (dm-6)      253:6    0    7G  0 lvm  /home
  └─os-usr_local (dm-7) 253:7    0    8G  0 lvm  /usr/local
sdb                       8:16   0   75G  0 disk
└─app-app (dm-2)        253:2    0   75G  0 lvm  /app

```

>The `findmnt` command allows you to display a list of currently mounted file systems.

```sh
$ findmnt
TARGET                       SOURCE                   FSTYPE      OPTIONS
/                            /dev/mapper/os-root      ext4        rw,relatime,barrier=1,data=ordered
├─/proc                      proc                     proc        rw,relatime
│ ├─/proc/bus/usb            /proc/bus/usb            usbfs       rw,relatime
│ └─/proc/sys/fs/binfmt_misc                          binfmt_misc rw,relatime
├─/sys                       sysfs                    sysfs       rw,relatime
├─/dev                       devtmpfs                 devtmpfs    rw,relatime,size=4017256k,nr_inodes=1004314,mode=755
│ ├─/dev/pts                 devpts                   devpts      rw,relatime,gid=5,mode=620,ptmxmode=000
│ └─/dev/shm                 tmpfs                    tmpfs       rw,relatime
├─/app                       /dev/mapper/app-app      ext4        rw,relatime,barrier=1,data=ordered
├─/boot                      /dev/sda1                ext4        rw,relatime,barrier=1,data=ordered
├─/home                      /dev/mapper/os-home      ext4        rw,relatime,barrier=1,data=ordered
├─/tmp                       /dev/mapper/os-tmp       ext4        rw,relatime,barrier=1,data=ordered
├─/usr                       /dev/mapper/os-usr       ext4        rw,relatime,barrier=1,data=ordered
│ └─/usr/local               /dev/mapper/os-usr_local ext4        rw,relatime,barrier=1,data=ordered
└─/var                       /dev/mapper/os-var       ext4        rw,relatime,barrier=1,data=ordered

```