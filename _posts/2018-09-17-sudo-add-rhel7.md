---
layout: post
title:  add user to sudo  -RHEL 7
categories: [RedHat7]
view: true
---

1. Login as root  
`sudo su`  
2. 
usermode -G wheel your_user_name
3. Logout, login
