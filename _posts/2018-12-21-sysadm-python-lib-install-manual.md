---
layout: post
title: Python lib install manual
categories: [SysAdmin]
view: true
tag: python
---
`**Steps:**`  

**Script Settup Lab**
```sh
1. Get lib file. Example: hashlib-20081119.zip
2. unzip hashlib-20081119.zip
3. cd hashlib-20081119/
4. export CC=gcc (if gcc not exist, please install it before going to next steps )
5. python setup.py build
6. sudo python setup.py install
7. check result: pip list | grep hashlib
```
