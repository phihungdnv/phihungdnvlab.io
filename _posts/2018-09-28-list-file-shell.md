---
layout: post
title: Listing Files- Linux
categories: [linux]
view: true
tag: ls
---
**Common options:**
`-a`,`--all` -> List all entries including ones that start with a dot  
`-A`, `--almost-all` -> List all entries excluding . and ..   
`-c` -> Sort files by change time  
`-h`, `--human-readable` -> Show size in human readable format (i.e K, M)  
`-l` -> show contents in log-listing format  
`-r`, `--reverse` -> Show contents in reverse order  
`-S` -> Sort by size   
`-t` -> Sort by modification time  
`-u` -> Sort by last access time  

**Example `-c`: List all files sort by change time**
```sh
[rhel7@vhost1 ~]$ for i in `ls -c`; do echo $i; done
test
arg.sh
getip.sh
```

**Example `-h`:**
```sh
[rhel7@vhost1 ~]$ ls -Alh | head -10
total 4.0M
drwxrwxr-x.  4 rhel7 rhel7   55 Sep 23 13:10 app
-rw-rw-r--.  1 rhel7 rhel7  239 Sep 28 10:18 arg.sh
-rw-------.  1 rhel7 rhel7  26K Sep 27 17:26 .bash_history
-rw-r--r--.  1 rhel7 rhel7   18 Mar  7  2017 .bash_logout
-rw-r--r--.  1 rhel7 rhel7  193 Mar  7  2017 .bash_profile
-rw-r--r--.  1 rhel7 rhel7  390 Sep 25 22:33 .bashrc
drwx------. 16 rhel7 rhel7 4.0K Sep 27 12:05 .cache
drwxr-xr-x. 14 rhel7 rhel7 4.0K Sep 27 12:45 .config
drwx------.  3 rhel7 rhel7   25 Sep 22 07:51 .dbus
```
**Example `-S`, `-r`: Sort by size with reverse**
```sh
[rhel7@vhost1 ~]$ ls -lS
total 3928
-rw-rw-r--. 1 rhel7 rhel7 3953518 Sep 26 14:33 wordpress.backup
-rw-r--r--. 1 rhel7 rhel7   19023 Sep 27 09:40 udo systemctl list-units --type service
-rw-r--r--. 1 rhel7 rhel7    8207 Sep 26 18:22 t installedqq
-rw-r--r--. 1 root  root     6926 Sep 27 09:41 udo systemctl list-units-file --type service
-rw-rw-r--. 1 rhel7 rhel7    6161 Sep 23 01:59 log
-rw-rw-r--. 1 rhel7 rhel7     239 Sep 28 10:18 arg.sh
-rwxr-xr-x. 1 root  root      195 Sep 17 21:04 hello.py

[rhel7@vhost1 ~]$ ls -lSr                                                                      
total 3928                                                                                     
-rw-r--r--. 1 rhel7 rhel7       0 Sep 26 15:08 somedata.log                                    ```
-rw-rw-r--. 1 rhel7 rhel7       4 Sep 28 10:31 test                                            
drwxr-xr-x. 2 rhel7 rhel7       6 Sep 17 17:01 Videos                                          
drwxr-xr-x. 2 rhel7 rhel7       6 Sep 17 17:01 Templates                                                                        
drwxrwxr-x. 2 rhel7 rhel7      24 Sep 22 16:30 httpd                                           
drwxrwxr-x. 3 rhel7 rhel7      40 Sep 21 23:14 local-httpd                                     
drwxrwxr-x. 4 rhel7 rhel7      55 Sep 23 13:10 app                                             
drwxr-xr-x. 3 rhel7 rhel7      75 Sep 17 17:14 Downloads                                       
-rw-rw-r--. 1 rhel7 rhel7     116 Sep 28 09:50 getip.sh                                        
-rwxr-xr-x. 1 root  root      195 Sep 17 21:04 hello.py                                        
-rw-rw-r--. 1 rhel7 rhel7     239 Sep 28 10:18 arg.sh                                          
-rw-rw-r--. 1 rhel7 rhel7    6161 Sep 23 01:59 log                                             
-rw-r--r--. 1 root  root     6926 Sep 27 09:41 udo systemctl list-units-file --type service    
-rw-r--r--. 1 rhel7 rhel7    8207 Sep 26 18:22 t installedqq                                   
-rw-r--r--. 1 rhel7 rhel7   19023 Sep 27 09:40 udo systemctl list-units --type service         
-rw-rw-r--. 1 rhel7 rhel7 3953518 Sep 26 14:33 wordpress.backup   
```                             