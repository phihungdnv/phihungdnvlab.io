---
layout: post
title: Redirection - Unix/Linux
categories: [linux]
view: true
---
Linux opens three file descriptors for the program:  
- 0 standard input (stdin)  
- 1 standard output (stdout)  
- 2 standard error (stderr)

There are two main output streams in Linux (and other OSs), standard output (stdout) and standard error (stderr). Error messages, like the ones you show, are printed to standard error. The classic redirection operator (command > file) only redirects standard output, so standard error is still shown on the terminal. To redirect stderr as well, you have a few choices:  
1. Redirect stdout to one file and stderr to another file:  
```sh
command > out 2>error
```
2. Redirect stderr to stdout (&1), and then redirect stdout to a file:  
```sh
command >out 2>&1
```
3. Redirect both to a file:  
```sh
command &> out
```
`/dev/null` is a black hole where any data sent, will be discarded. Therefore `>/dev/null 2>&1` is redirect the output of your program to /dev/null. Include both the Standard Error and Standard Out.

```sh
root@pentest:~/log# ls access_http.{2017-{05..12}-{01..30},2018-{01..03}-{01..10},2018-09-{07..09}}
ls: cannot access 'access_http.2018-02-05': No such file or directory -> stderr
ls: cannot access 'access_http.2018-02-06': No such file or directory
ls: cannot access 'access_http.2018-02-07': No such file or directory
ls: cannot access 'access_http.2018-02-08': No such file or directory
ls: cannot access 'access_http.2018-02-09': No such file or directory
ls: cannot access 'access_http.2018-02-10': No such file or directory
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-01
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-02 -> stdout
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-03
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-04
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-05
```
**Move error to /dev/null**
```sh
root@pentest:~/log# ls access_http.{2017-{05..12}-{01..30},2018-{01..03}-{01..10},2018-09-{07..09}} -l 2>/dev/null
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-01
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-02
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-03
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-04
-rw-r--r-- 1 root root 0 Sep  9 16:59 access_http.2017-05-05
-rw-r--r-- 1 root root 0 Sep  9 17:20 access_http.2017-08-25
-rw-r--r-- 1 root root 0 Sep  9 17:20 access_http.2017-08-26
-rw-r--r-- 1 root root 0 Sep  9 17:20 access_http.2017-08-27
-rw-r--r-- 1 root root 0 Sep  9 17:20 access_http.2017-08-28
```
**Move stdout to file**
```sh
root@pentest:~/log# ls access_http.{2017-{05..12}-{01..30},2018-{01..03}-{01..10},2018-09-{07..09}} -l 1>logoke.md
ls: cannot access 'access_http.2017-05-06': No such file or directory
ls: cannot access 'access_http.2017-05-07': No such file or directory
ls: cannot access 'access_http.2017-05-08': No such file or directory
ls: cannot access 'access_http.2017-05-09': No such file or directory
ls: cannot access 'access_http.2017-05-10': No such file or directory
```
