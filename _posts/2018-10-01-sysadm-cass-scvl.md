---
layout: post
title: SysAdm| Export CSV Cassandra using cqlsh and Python
categories: [SysAdmin]
view: true
---

```sh
#!/usr/bin/env bash
#
#Usage example: ./get_data.sh --snid 96fe0c6d-f147-4a5b-ad73-3870de1382eb --subid a63332d6-4fd5-4887-8952-b30778e26b20
#
usage="bash $0 --snid <snid> --subid <subscriptionid>"

if [[ $# == 0 ]]; then 
	echo $usage
	exit 1;
fi

while (($#)); do case $1 in
	--snid)
	    snid=$2;;
	--subid)
		subid=$2;;
	*)
		echo $usage
		exit 1
	esac; shift 2
done

cql_state="SELECT snid,subscriptionid,utctime,usageinbytes FROM sapphire.usagereportplanindex WHERE snid='$snid' and usagetype=4 and subscriptionid='$subid';"

cqlsh -u cass_prod_read cass01.di01.sapp -preadcassonly -e "$cql_state" > tmp.data 

cat tmp.data | grep -v rows |tail -n +4 | awk '{print $1,$3,$5,$7}'| sed '/^\s*$/d' > output.txt

cat <<END > export.py
import csv
import math
tmp=[]
with open("output.txt", mode = 'r') as f_data:
	csv_r = csv.reader(f_data,delimiter=' ')
	with open('report.csv',mode = 'w') as f_report:
		fieldnames = ['snid', 'subscription', 'utctime', 'usageinbyte', 'usageinMB', 'usageinGB']
		wr = csv.DictWriter(f_report,fieldnames=fieldnames)
		wr. writeheader()
		for row in csv_r:
			wr.writerow({"snid": row[0],"subscription": row[1], "utctime": row[2], \
			"usageinbyte": row[3], "usageinMB": round((float(row[3])/(math.pow(1024,2))),2), \
			"usageinGB": round((float(row[3])/(math.pow(1024,3))),2)})
END
python2 export.py
mkdir log
mv -t log  export.py output.txt tmp.data
echo "Done!"
```
