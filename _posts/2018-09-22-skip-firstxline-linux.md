---
layout: post
title: Print a file skipping first X lines in Bash linux
categories: [linux]
view: true
tag: tail
---
`SKIP a particular number of lines`
Example:
```sh
[root@localhost rhel7]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                    PORTS               NAMES
c9ef75dd4bf0        968384e7a2cc        "/usr/sbin/httpd -..."   9 hours ago         Exited (1) 9 hours ago                        log.app
9045e880eb48        06144b287844        "bash"                   2 days ago          Exited (0) 2 days ago                         nginx.app
d59c03606640        f5ea21241da8        "bash"                   2 days ago          Exited (1) 2 days ago                         reverent_goldstine
c7e1e24494a6        968384e7a2cc        "/usr/sbin/httpd -..."   2 days ago          Exited (0) 10 hours ago                       httpd.app
```
Grep all `CONTAINER ID`
```sh
[root@localhost rhel7]# docker ps -a | awk '{print $1}'| tail -n +2
c9ef75dd4bf0
9045e880eb48
d59c03606640
c7e1e24494a6
```
