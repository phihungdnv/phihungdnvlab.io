---
layout: post
title: Brace expansion - Unix/Linux
categories: [linux]
view: true
---
```sh
root@pentest:~/linux# touch file{0..9}
root@pentest:~/linux# ll
total 8
drwxr-xr-x  2 root root 4096 Aug 27 17:20 ./
drwx------ 47 root root 4096 Aug 27 17:17 ../
-rw-r--r--  1 root root    0 Aug 27 17:20 file0
-rw-r--r--  1 root root    0 Aug 27 17:20 file1
-rw-r--r--  1 root root    0 Aug 27 17:20 file2
-rw-r--r--  1 root root    0 Aug 27 17:20 file3
-rw-r--r--  1 root root    0 Aug 27 17:20 file4
-rw-r--r--  1 root root    0 Aug 27 17:20 file5
-rw-r--r--  1 root root    0 Aug 27 17:20 file6
-rw-r--r--  1 root root    0 Aug 27 17:20 file7
-rw-r--r--  1 root root    0 Aug 27 17:20 file8
-rw-r--r--  1 root root    0 Aug 27 17:20 file9
```
```sh
root@pentest:~/linux# mkdir folder{0,1,2}{a,b,c}
root@pentest:~/linux# ll
total 52
drwxr-xr-x 11 root root 12288 Aug 27 17:24 ./
drwx------ 47 root root  4096 Aug 27 17:17 ../
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder0a/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder0b/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder0c/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder1a/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder1b/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder1c/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder2a/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder2b/
drwxr-xr-x  2 root root  4096 Aug 27 17:24 folder2c/
```

```
{aa,bb,cc,dd}  => aa bb cc dd
{0..12}        => 0 1 2 3 4 5 6 7 8 9 10 11 12
{3..-2}        => 3 2 1 0 -1 -2
{a..g}         => a b c d e f g
{g..a}         => g f e d c b a
a{0..3}b       => a0b a1b a2b a3b
{a,b{1..3},c}  => a b1 b2 b3 c


2018-08-26
2018-09-08
2018-{08-{26..31},09-{01-08}}

cp /a/really/long/path/to/some/file.txt{,.bak}
copy /a/really/long/path/to/some/file.txt to /a/really/long/path/to/some/file.txt.bak
```
**range date**
```
2017-05-01 -> 05
2018-09-07 -> 09

access_http.{2017-05-{01..05},2018-09-{07..09}}

2017-05-01 -> 2018-03-10
2018-09-07 -> 09

access_http.{2017-{05..12}-{01..30},2018-{01..03}-{01..10},2018-09-{07..09}}


2017-05-07, 2017-05-15 - 2017-08-25, 2018-03-11/20, 2018-03-26 - 2018-09-09

access_http.{2017-05-07,2017-{05-{15..31},{06..07}-{01..30},08-{01..25}},2018-01-{11..201},2018-{03-{26..31},{04..08}-{01..30},09-{01..09}}}
```

