---
layout: post
title: read/write file python
categories: [python]
---
**Example 1**  
`maillist.txt` content
```
phihungdnv@gmail.com				
hung.ngo@evizi.com   				
tunayvesau@gmail.com
python@gmail.com
```

```python
with open('maillist.txt') as f:
	#lines = f.read().splitlines()
	mails=[lines.strip() for lines in f]
	print (mails)
```
output
```
['phihungdnv@gmail.com', 'hung.ngo@evizi.com', 'tunayvesau@gmail.com', 'python@gmail.com']
```
**Example 2**  
`maillist2.txt` content
```python
with open('maillist2.txt') as f:
    mails = f.read();
    list_mails = mails.split(',')
print(list_mails)
```
output
```
['phihungdnv@gmail.com', 'hung.ngo@gmail.com', 'iloveyou@gmail.com', 'python@gmail.com']
```