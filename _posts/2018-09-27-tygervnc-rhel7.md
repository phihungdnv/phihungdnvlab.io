---
layout: post
title:  Tiger Virtual Network Computing(TIGERVNC) - RHEL 7
categories: [RedHat7]
view: true
tag: vnc
---
>TigerVNC (Tiger Virtual Network Computing) is a system for graphical desktop sharing which allows you to remotely control other computers.

>TigerVNC works on the client-server principle: a server shares its output (`vncserver`) and a client (`vncviewer`) connects to the server.

### NOTE
```
Unlike in previous Red Hat Enterprise Linux distributions, TigerVNC in Red Hat Enterprise Linux 7 uses the systemd system management daemon for its configuration.
The /etc/sysconfig/vncserver configuration file has been replaced by /etc/systemd/system/vncserver@.service.
```

### VNC Server
**Install VNC Server**
```sh
$ sudo yum -y install tigervnc-server
```
**Configuring VNC Server**
```sh
$ sudo cp /usr/lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@.service
```
**Edit `/etc/systemd/system/vncserver@.service`, replacing `<USER>` with the actual username. Here are example:**
```sh
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
Type=forking

# Clean any existing files in /tmp/.X11-unix environment
ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill %i -geometry 1280x1024 > /dev/null 2>&1 || :'
ExecStart=/usr/sbin/runuser -l rhel7 -c "/usr/bin/vncserver %i"
PIDFile=/home/rhel7/.vnc/%H%i.pid
ExecStop=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'

[Install]
WantedBy=multi-user.target
```
The `-geometry` argument specifies the size of the VNC desktop to be created; by default, it is set to 1024x768.
**To make the changes take effect immediately, issue the following command:**
```sh
$ sudo systemctl daemon-reload
```
**Set the password for the user or users defined in the configuration file. Note that you need to switch from `root` to USER first.**
```sh
[rhel7@172 ~]$ vncpasswd
Password:
Verify:
Would you like to enter a view-only password (y/n)? n
A view-only password is not used
```
**Configuring VNC Server for multi Users**
Example: Create two service files, then replaced username in config
```sh
$ sudo cp /etc/systemd/system/vncserver@.service /etc/systemd/system/vncserver-phihung@.service
$ sudo cp /etc/systemd/system/vncserver@.service /etc/systemd/system/vncserver-trunghieu@.service
```
Set passwords for both users:
```sh
[phihung@172 rhel7]$ vncpasswd
Password:
Verify:
Would you like to enter a view-only password (y/n)? n
A view-only password is not used
----
[root@172 rhel7]# su trunghieu
[trunghieu@172 rhel7]$ vncpasswd
Password:
Verify:
Would you like to enter a view-only password (y/n)? n
A view-only password is not used
```
Reload daemon
```sh
$ sudo systemctl daemon-reload
```
**Firewall config**
```sh
$ sudo firewall-cmd --permanent --add-port=5903/tcp
$ sudo firewall-cmd --permanent --add-port=6003/tcp
$ sudo firewall-cmd --reload
```
**Starting VNC Server**
`sudo systemctl start vncserver@:<display_number>.service`
```sh
$ sudo systemctl start vncserver@:3.service
$ sudo systemctl start vncserver-phihung@:5.service
```
**Check status**
```sh
$ systemctl status vncserver@:3.service
$ systemctl status vncserver-phihung@:5.service
```
### Noded: 
`<display_number>` 3 -> vncport: 5903  
`<display_number>` 5 -> vncport: 5905  
**Check port listenning**
```sh
[root@172 rhel7]# netstat -antp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:5905            0.0.0.0:*               LISTEN      6713/Xvnc
tcp        0      0 0.0.0.0:6005            0.0.0.0:*               LISTEN      6713/Xvnc
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      1808/dnsmasq
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1488/sshd
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1487/cupsd
tcp        0      0 172.16.20.132:22        172.16.20.1:64500       ESTABLISHED 3295/sshd: rhel7 [p
tcp6       0      0 :::5905                 :::*                    LISTEN      6713/Xvnc
tcp6       0      0 :::6005                 :::*                    LISTEN      6713/Xvnc
tcp6       0      0 :::22                   :::*                    LISTEN      1488/sshd
tcp6       0      0 ::1:631                 :::*                    LISTEN      1487/cupsd
```
**Client access with ip and port by using tigervnc view or different tools**

