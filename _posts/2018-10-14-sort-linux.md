---
layout: post
title: Sort - Linux
categories: [linux]
view: true
tag: sort
---
**Example 1: Check disk usage space sort by usage **
```sh
$ df -Ph | sort -k5 -g
Filesystem                Size  Used Avail Use% Mounted on
tmpfs                     3.9G     0  3.9G   0% /dev/shm
/dev/mapper/os-tmp        2.0G  3.2M  1.9G   1% /tmp
/dev/mapper/os-usr_local  7.8G  736M  6.7G  10% /usr/local
/dev/sda1                 477M   65M  387M  15% /boot
/dev/mapper/os-root       7.8G  1.4G  6.0G  19% /
/dev/mapper/os-var         11G  2.7G  7.1G  28% /var
/dev/mapper/os-usr        9.8G  2.8G  6.5G  31% /usr
/dev/mapper/os-home       6.8G  3.0G  3.5G  46% /home
/dev/mapper/app-app        74G   64G  6.8G  91% /app
```
**Example 2: Get first and last line**
```sh
$ df -Ph | sort -gk5 | awk 'NR==1;END {print}'
Filesystem                Size  Used Avail Use% Mounted on
/dev/mapper/app-app        74G   65G  5.4G  93% /app
```
`options:`  
```
-k <number colums>
-g Compare according to general numerical value
-M mount sort
-r reverse
-n / -g  sort the file with numeric data present inside
-u sort and remove duplicates
-h human readable numberic values
```
**Example3**
```
19.09.1942 12:34              Nekros
05.03.1945 20:15              Inaros
01.01.1969 23:55              Banshee
02.01.1969 20:15              Mesa
03.01.1969 12:00              Nezha
19.02.1969 17:38              Nova
06.03.1969 07:45              Excalibur
01.11.1969 08:00              Hydroid
02.09.1970 10:15              Rhino
29.07.1973 09:10              Atlas
08.04.2007 05:10              Wukong
```
**Sort by year**
```sh
$ cat year | sort -k1.7n
19.09.1942 12:34              Nekros
05.03.1945 20:15              Inaros
01.01.1969 23:55              Banshee
01.11.1969 08:00              Hydroid
02.01.1969 20:15              Mesa
03.01.1969 12:00              Nezha
06.03.1969 07:45              Excalibur
19.02.1969 17:38              Nova
02.09.1970 10:15              Rhino
29.07.1973 09:10              Atlas
08.04.2007 05:10              Wukong
```
colum 1, from character 7, sort by numberic    
**Sort by year and month**
```sh
]$ cat year | sort -k 1.7n -k 1.4,1.5n
19.09.1942 12:34              Nekros
05.03.1945 20:15              Inaros
01.01.1969 23:55              Banshee
02.01.1969 20:15              Mesa
03.01.1969 12:00              Nezha
19.02.1969 17:38              Nova
06.03.1969 07:45              Excalibur
01.11.1969 08:00              Hydroid
02.09.1970 10:15              Rhino
29.07.1973 09:10              Atlas
08.04.2007 05:10              Wukong
```
`-k 1.4,1.5n`  -> Range colum 4 - 5

`References:`  
https://www.larshaendler.com/2017/01/18/use-sort-k-option-flag-for-advanced-linux-command-line-sorting/
