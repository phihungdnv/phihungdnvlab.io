---
layout: post
title: Sharing Data Between Containers - Rhel7 [15] - DOCKER
categories: [docker]
view: true
tag: docker
---
By default, Docker mounts the volume in read-write mode. To mount the previous working directory to /cookbook as read-only, you would use -v "$PWD":/cookbook:`ro`  

**Run a `data` container, create `info_member.txt`, `info_member.txt` in volume inside container `workspace`:**
```sh
[rhel7@172 ~]$ docker run --name data -it -v /workspace ubuntu:14.04 bash
root@58308c7de28b:/# touch workspace/info_member.txt
root@58308c7de28b:/# touch workspace/info_employee.txt
root@58308c7de28b:/# exit
```
**Inspect your mount mapping**
```sh
$ docker inspect 58308c7de28b | grep volume
[{volume f9e1586edbebbc790161d501c2d08a07db61cf06ff3b975fe56d56eeb7bdab30 /var/lib/docker/volumes/f9e1586edbebbc790161d501c2d08a07db61cf06ff3b975fe56d56eeb7bdab30/_data /workspace local  true }]
```
```sh
[rhel7@172 ~]$ sudo ls -l /var/lib/docker/volumes/f9e1586edbebbc790161d501c2d08a07db61cf06ff3b975fe56d56eeb7bdab30/_data
total 0
-rw-r--r--. 1 root root 0 Sep 26 14:49 info_employee.txt
-rw-r--r--. 1 root root 0 Sep 26 14:49 info_member.txt
```
**Mount the volume from `data` container exist with the --volumes-from option:**
```sh
$ docker run --name getdata --volumes-from data -it ubuntu:14.04 bash
root@404145f222fa:/# ls | grep workspace
workspace
```
**Create data**
```sh
root@404145f222fa:/workspace# touch getdata.md
root@404145f222fa:/workspace# ll
total 0
drwxr-xr-x. 2 root root 72 Sep 26 07:59 ./
drwxr-xr-x. 1 root root 34 Sep 26 07:57 ../
-rw-r--r--. 1 root root  0 Sep 26 07:59 getdata.md
-rw-r--r--. 1 root root  0 Sep 26 07:49 info_employee.txt
-rw-r--r--. 1 root root  0 Sep 26 07:49 info_member.txt
root@404145f222fa:/workspace# exit
```
**Check ...**
```sh
[rhel7@172 ~]$ sudo ls -l /var/lib/docker/volumes/f9e1586edbebbc790161d501c2d08a07db61cf06ff3b975fe56d56eeb7bdab30/_data
[sudo] password for rhel7:
total 0
-rw-r--r--. 1 root root 0 Sep 26 14:59 getdata.md
-rw-r--r--. 1 root root 0 Sep 26 14:49 info_employee.txt
-rw-r--r--. 1 root root 0 Sep 26 14:49 info_member.txt
```




