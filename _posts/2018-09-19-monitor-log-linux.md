---
layout: post
title: Monitor Log Files in Real Time - linux
categories: [linux]
view: true
tag: tail
---
**Option 1**  
`tail -f path_to_file.log`
```sh
[root@c7e1e24494a6 httpd]# tail -f access_log
172.16.20.1 - - [19/Sep/2018:17:49:07 +0000] "GET / HTTP/1.1" 403 3985 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"

172.16.20.1 - - [19/Sep/2018:17:52:09 +0000] "GET /admin HTTP/1.1" 404 203 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
172.16.20.1 - - [19/Sep/2018:17:53:12 +0000] "GET /admin HTTP/1.1" 404 203 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
172.16.20.1 - - [19/Sep/2018:17:53:14 +0000] "GET / HTTP/1.1" 403 3985 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"

172.16.20.1 - - [19/Sep/2018:17:53:15 +0000] "GET / HTTP/1.1" 403 3985 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"

172.16.20.1 - - [19/Sep/2018:17:53:32 +0000] "GET /phihung HTTP/1.1" 404 205 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
```

**Option 2**  
`tailf path_to_file.log`

**Option 3**  
 `tail` command will display the last 10 lines of a file. Option `-n <num_line>` to set line which log output    
 `tailf -n2 path_to_file.log`  
 ```sh
[root@c7e1e24494a6 httpd]# tailf -n2 access_log
172.16.20.1 - - [19/Sep/2018:17:56:17 +0000] "GET /phihung HTTP/1.1" 404 205 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
172.16.20.1 - - [19/Sep/2018:17:56:23 +0000] "GET /hhhhhhh HTTP/1.1" 404 205 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
 ```


