---
layout: post
title:  If statement - Shell
categories: [shell]
view: true
---

**Script Example**
```sh
if [[ $1 -eq 5 ]]; then
	echo "$1 = 5"
elif [[  $1 -gt 10 ]]; then
	echo "$1 >= 10"
else 
	echo "NOTHING"
fi
```
`Executed Script`  
```sh
[rhel7@vhost1 ~]$ bash ifstatement.sh 10
NOTHING
[rhel7@vhost1 ~]$ bash ifstatement.sh
NOTHING
[rhel7@vhost1 ~]$ bash ifstatement.sh 5
5 = 5
[rhel7@vhost1 ~]$ bash ifstatement.sh 10
NOTHING
[rhel7@vhost1 ~]$ bash ifstatement.sh 1011
1011 >= 10
```
  

