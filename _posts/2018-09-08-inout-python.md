---
layout: post
title: input/output python3
categories: [python]
---
`f` or `F` is corect
```python
>>> fullname = 'ngo phi hung'
>>> print(f'Fullname is {fullname}')
Fullname is ngo phi hung
>>> print(F'Fullname is {fullname}')
Fullname is ngo phi hung
>>> tmp = f'Full name is {fullname}'
>>> tmp
'Full name is ngo phi hung'
```

String format() method
```python
>>> print('We are the {} who say "{}!"'.format('knights', 'Ni'))
We are the knights who say "Ni!"

>>> print('{0} and {1}'.format('spam', 'eggs'))
spam and eggs
>>> print('{1} and {0}'.format('spam', 'eggs'))
eggs and spam
```
