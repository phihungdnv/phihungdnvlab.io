---
layout: post
title: Docker commit - Rhel7 [17] - DOCKER
categories: [docker]
view: true
tag: docker
---
### Example  
**Start a container with an interactive bash shell and update the packages in it:**
```sh
$ docker run --name update_img -it ubuntu:14.04 bash
root@ae22ae3c3f94:/# apt-get update
Get:1 http://security.ubuntu.com trusty-security InRelease [65.9 kB]
Ign http://archive.ubuntu.com trusty InRelease
Get:2 http://archive.ubuntu.com trusty-updates InRelease [65.9 kB]
...
Fetched 21.5 MB in 1min 44s (205 kB/s)
Reading package lists... Done
root@ae22ae3c3f94:/# exit
```
**Commit all changed**
```sh
$ docker commit ae22ae3c3f94 ubuntu:v1
sha256:4a8451a09105d7dcdded1a131aea0bace89d1025c2ebaa2de60da5d1ee8fde17
$ docker images
REPOSITORY                                        TAG                 IMAGE ID            CREATED             SIZE
ubuntu                                            v1                  4a8451a09105        10 seconds ago      210 MB
...
```
**Inspect the changes that have been made inside this container**
```sh
$ docker diff ae22ae3c3f94
```
```
A: the file or directory listed was added
C: there was a change made
D: means that it was deleted.
```

