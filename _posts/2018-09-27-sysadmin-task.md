---
layout: post
title:  SysAdmin tasks
categories: [shell]
view: true
---

`Preinstallation`: Assigining an IP address, configuring existing servers and network services and so on.  
`Installation`: Installing a new operating system and preparing it for automation.  
`Configuration`: Performing initial configuration and reconfiguration tasks.  
`Managing data`: Duplicationg or sharing data.  
`Maintenance and changes`: Rotating logs, adding accounts, and so on.  
`Installing/ upgrading sofware`: Using package management and/ or custom distri-bution methods  
`System monitoring and security`: Performing log analysis and security scans; monitoring system load, disk space, drive failures, and so on.  
