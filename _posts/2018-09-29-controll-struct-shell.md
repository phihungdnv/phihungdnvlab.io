---
layout: post
title: Control Structures - Shell
categories: [shell]
view: true
---
**File Operators**
```sh
-e "$file" -> Returns true if the file exits.
-d "$file" -> Returns true if the file exits and is a directory.
-f "$file" -> Returns true if the file exits and is a regular file
-h "$file" -> Returns true if the file exits and is a symbolic link
```
**String Comparators**
```sh
-z "$str" -> True if length of string zero
-n "$str" -> True if length of string non-zero
```
**Integer Comparators**
```sh
"$int1" -eq "$int2" -> True if the intergers are equal
"$int1" -ne "$int2" -> True if the interfers are not equal
"$int1" -gt "$int2" -> True if int1 > int2
"$int1" -ge "$int2" -> True if int1 >= int2
"$int1" -lt "$int2" -> True if int1 < int2
"$int1" -le "$int2" -> True if int1 <= int2
```

**Example**
```sh
$ ll | grep wordpress.backup
-rw-rw-r--. 1 rhel7 rhel7 3953518 Sep 26 14:33 wordpress.backup

$ if [[ -e wordpress.backup ]]; then echo "File exist"; fi
File exist
```
   