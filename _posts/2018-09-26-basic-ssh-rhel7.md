---
layout: post
title:  SSH Basic - RHEL 7
categories: [RedHat7]
view: true
---
**Working directory and permission**
```sh
$ chmod 600 $HOME/.ssh
$ chmod 600 $HOME/.ssh/config
$ chmod 644 $HOME/.ssh/id_rsa.pub
$ chmod 600 $HOME/.ssh/id_rsa
$ chmod 600 $HOME/.ssh/authorized_keys
```
`Explain:`
```
`.ssh`: ssh container
`config`: config for ssh `client` connect to ssh `server`
`id_rsa`: private key
`id_rsa.pub`: public key
`authorized_keys`: public key
```
**Creating the key files and Copying them to the Server**
```sh
$ ssh-keygen -t rsa -b <1024|2048>
```
**Copy public to remote server**
```sh
$ ssh-copy-id -i <public_key> USER@remote_host_ip
```

