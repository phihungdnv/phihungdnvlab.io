---
layout: post
title:  Importance of Quoting in Strings - Shell
categories: [shell]
view: true
---
There are two types of quoting:  
- `weak`: uses double qoutes `\"` 
- `strong`: use single qoutes `\``
Example output:  
```sh
[rhel7@vhost1 ~]$ echo "$HOME"
/home/rhel7
```
```sh
[rhel7@vhost1 ~]$ echo '$HOME'
$HOME
```

