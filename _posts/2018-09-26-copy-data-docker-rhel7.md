---
layout: post
title: Copying Data to and from Containers without mount volume - Rhel7 [16] - DOCKER
categories: [docker]
view: true
tag: docker
---
**Run a container without mount volume**
```sh
$ docker run --name get_copy -it ubuntu:14.04 bash
```
```sh
root@bb2dcb9b869d:~/data# pwd
/root/data
root@bb2dcb9b869d:~/data# ll
total 0
drwxr-xr-x. 2 root root 26 Sep 26 08:08 ./
drwx------. 1 root root 38 Sep 26 08:08 ../
-rw-r--r--. 1 root root  0 Sep 26 08:08 somedata.log
```

**Copy `somedata.log` file in `/root/data` from `get_copy` containers to localhost**
```sh
$ docker cp get_copy:/root/data/somedata.log $HOME
$ ll | grep somedata
-rw-r--r--. 1 rhel7 rhel7       0 Sep 26 15:08 somedata.log
```
