---
layout: post
title: Type of Zabbix monitoring
categories: [zabbix]
view: true
tag: zabbix
--- 

**Active agents:** Zb Agent pull items have to check from Zb Server -> generate data  -> push to Zb server  
`Values in agent config:`
```sh
ServerActive <ip_zabbix_server>,<zabbix2>
#how many times the agent has to ask for this list
RefreshActiveChecks 120
#The standard value that data is kept is 5 seconds but can be increased up to 1 hour 
BufferSend 5

```  
**Passive agent:** Passive checks are really simple in the way that the server or proxy will ask the agent for some data such as CPU load, disk space, and so on. The Zabbix agent will then give the requested data back to the Zabbix server or proxy.  
`Values in agent config:`  
```sh
Server <zabbix_server>
```
`Active vs Passive agents:` there is more communication between the passive agent and the server
than with the active agent and the server. This means that more sockets will be opened
on the server side. So in a large setup, you could possibly run out of sockets if you have
a lot of passive agents running without a proxy. Also, the passive agent has no buffer
such as the active agent.

**Extending Agents**: Extend agent with user `parameters`  
The `UserParameter` option that we put in the agent config file has the following syntax:
```
UserParameter=<key>,<command>
```
`example`:
```sh
UserParameter = some.key[*],somescript.sh $1 $2
# * --> determine an unlimited number of items starting with some.key parameter when we create our item in the Zabbix server
```
Defined key on Server: some.key['x','y']  
Normally `UserParameter` will be the user `zabbix`. Sometimes the command you want to execute needs root privileges. 
```sh
root@x:# echo 'zabbix ALL=NOPASSWD:ALL' >> /etc/sudoer
```

**Simple checks**: Simple checks are checks that can be run from the Zabbix server without the need of a Zabbix agent on the
host. Checks by ICMP ping or by port scan if a host is online and whether the service accepts connections.  

**SNMP checks:** SNMP protocol  ...   

**Internal checks:** The Zabbix internal items are items Zabbix can monitor to give us some insights on what's going on under the hood. We don't need an agent, make sure that you have super administration rights.

`Zabbix internal processes`: ps -ef | grep zabbix_server    


```sh
Alerter: This is the process responsible for sending messages.
Configuration syncer: This is responsible for loading configuration data from the
database into the cache.
DB watchdog: This will check if our database is available and log this when it is
not the case.
Discoverer: This process will scan the network (autodiscovery).
Escalator: This is process the escalations.
History syncer: This process writes data collected into the database.
HTTP poller: This is the process needed for Website Monitoring (scenarios).
Housekeeper: This process that deletes old data from the database.
ICMP pinger: This process is responsible for the ping of hosts.
IPMI poller: This process will collect data via IPMI.
Node watcher: The process is responsible for data exchange in a distributed
monitoring (deprecated since 2.4).
Self-monitoring: The process is responsible for the collection of internal data.
Poller: The process responsible for the collection of data from Zabbix Agent
(passive) and SNMP.
Proxy poller: The process is responsible for collecting data from passive proxies.
Timer: This process will execute the time-dependent triggers ( nodata () ).
Trapper: This process will accept all incoming data of active agents, Zabbix
sender and active proxies.
Unreachable: This service will contact unreachable hosts to see if they might be
available again
```

**Zabbix trapperz:** Zabbix provides `zabbix_sender`, a tool to send data that we have
gathered by, for example, our own scripts. This data will then be sent to the Zabbix
server. The data sent to the server will be gathered by the `Zabbix trapper`  

`Usage`:
```sh
zabbix_sender -z <ip-zabbixserver> -s <hostname agent> -k <item_key> -o <value>
```
```

