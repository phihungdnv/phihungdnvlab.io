---
layout: post
title: Investigating docker image Red Hat Enterprise Linux [3] - DOCKER
categories: [docker]
view: true
tag: docker
---
[*] List images on your local system  
`$ docker images`  
```
[rhel7@localhost ~]$ docker images
REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
docker.io/hello-world                   latest              4ab4c602aa5e        10 days ago         1.84 kB
docker.io/ubuntu                        latest              cd6d8154f1e1        12 days ago         84.1 MB
registry.access.redhat.com/rhel7/rhel   latest              f5ea21241da8        5 weeks ago         201 MB
```