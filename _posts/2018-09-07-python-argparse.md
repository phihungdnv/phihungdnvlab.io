---
layout: post
title: python argument
categories: [python]
view: true
---
**Python3 argparse example**
```python
import argparse
import sys
def ping(ip):
	for i in range(3):
		print('ping %s [%d]'%(ip,i))
def domain(ip):
	print('%s is gooogle.com'%(ip))
def portscan(port,ip):
	if port == 'wellknow_port':
		print('scan all wellknow port ...')
		print('80,81,82,...')
	else:
		for i in range(3):
			print('scan Port %s of %s'%(port,ip))
parser = argparse.ArgumentParser()
parser.add_argument('--host', help='ip ex:10.10.10.1 ')
parser.add_argument('--ping', help='ping ip',action='store_true')
parser.add_argument('--dns', help='dns revert', action='store_true')
parser.add_argument('--port', nargs='?', const='wellknow_port', type=str, help='port scan')

args = parser.parse_args()
if not args.host:
	print('[*] Missing --host argument')
if args.ping:
	ping(args.host)
if args.dns:
	domain(args.host)
if args.port:
	portscan(args.port,args.host)
``` 
**Output**
```sh
root@pentest:~# python3 opt.py
[*] Missing --host argument
```
```sh
root@pentest:~# python3 opt.py -h
usage: opt.py [-h] [--host HOST] [--ping] [--dns] [--port [PORT]]

optional arguments:
  -h, --help     show this help message and exit
  --host HOST    ip ex:10.10.10.1
  --ping         ping ip
  --dns          dns revert
  --port [PORT]  port scan
```
root@pentest:~# python3 opt.py --ping --host 8.8.8.8
ping 8.8.8.8 [0]
ping 8.8.8.8 [1]
ping 8.8.8.8 [2]
```
```sh
root@pentest:~# python3 opt.py --dns --host 8.8.8.8
8.8.8.8 is gooogle.com
```
```sh
root@pentest:~# python3 opt.py --port --host 8.8.8.8
scan all wellknow port ...
80,81,82,...
```
```sh
root@pentest:~# python3 opt.py --port 8080,9999 --host 8.8.8.8
scan Port 8080,9999 of 8.8.8.8
scan Port 8080,9999 of 8.8.8.8
scan Port 8080,9999 of 8.8.8.8
```

**Conceptual grouping of arguments with argparse.add_argument_group()**
```python
import argparse
parser = argparse.ArgumentParser(description='Simple example')
parser.add_argument('name', help='Who to greet', default='World')
# Create two argument groups
foo_group = parser.add_argument_group(title='Foo options')
bar_group = parser.add_argument_group(title='Bar options')
# Add arguments to those groups
foo_group.add_argument('--bar_this')
foo_group.add_argument('--bar_that')
bar_group.add_argument('--foo_this')
bar_group.add_argument('--foo_that')
args = parser.parse_args()
```
```sh
root@pentest:~# python grp.py --help
usage: grp.py [-h] [--bar_this BAR_THIS] [--bar_that BAR_THAT]
              [--foo_this FOO_THIS] [--foo_that FOO_THAT]
              name

Simple example

positional arguments:
  name                 Who to greet

optional arguments:
  -h, --help           show this help message and exit

Foo options:
  --bar_this BAR_THIS
  --bar_that BAR_THAT

Bar options:
  --foo_this FOO_THIS
  --foo_that FOO_THAT
```
```python
import pymysql
import argparse
import sys,getopt
dbhost = "127.0.0.1"
dbuser = "root"
dbpass = "@Lavie1799"
dbname = "dblavie"

def getConnection():
	conn = pymysql.connect(dbhost,dbuser,dbpass,dbname)
	return conn
def cursor():
	return  getConnection().cursor()
def listTables():
	data = cursor()
	sql = 'SHOW TABLES'
	data.execute(sql)
	for row in data:
			print (row)
def listFieldOfTable(tbname):
	data = cursor()
	sql = "SHOW COLUMNS FROM "+tbname+""
	data.execute(sql)
	for row in data:
			print (row)
getConnection()
parser = argparse.ArgumentParser(description='subparser example')
# Create Groups
list_group= parser.add_argument_group(title='List options')
create_group = parser.add_argument_group(title='Create options')
update_group = parser.add_argument_group(title='Update options')
# Add argument to these groups
list_group.add_argument('--list-tables',dest='listTable',action='store_true',help='list all table of database')
list_group.add_argument('--list-fields',dest='field',action='store_true',help='List all field of database')
list_group.add_argument('--table-name',metavar='table',dest='tableName',help='get table name')

create_group.add_argument('--create-table',metavar='table',dest='createTable',help='Create new table')

update_group.add_argument('--update_group',metavar='table',dest='updateTable',help='Update table')

args = parser.parse_args()
if args.listTable:
	listTables()
if args.tableTableField:
	listFieldOfTable(args.tableName)
```
```sh
root@pentest:~# python3 pq.py -h
usage: pq.py [-h] [--list-tables] [--list-table-fields] [--table-name table]
             [--create-table table] [--update_group table]

subparser example

optional arguments:
  -h, --help            show this help message and exit

List options:
  --list-tables         List all table of database
  --list-table-fields   List all field of table
  --table-name table    get table name

Create options:
  --create-table table  Create new table

Update options:
  --update_group table  Update table
  
 root@pentest:~# python3 pq.py --list-tables
('wp_commentmeta',)
('wp_comments',)
('wp_links',)
('wp_options',)
('wp_postmeta',)
('wp_posts',)
('wp_term_relationships',)
('wp_term_taxonomy',)
('wp_termmeta',)
('wp_terms',)
('wp_usermeta',)
('wp_users',)

root@pentest:~# python3 pq.py  --list-table-fields  --table-name wp_terms
('term_id', 'bigint(20) unsigned', 'NO', 'PRI', None, 'auto_increment')
('name', 'varchar(200)', 'NO', 'MUL', '', '')
('slug', 'varchar(200)', 'NO', 'MUL', '', '')
('term_group', 'bigint(10)', 'NO', '', '0', '')
```

**subparser**
```python
import argparse

parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers(help='commands')

# A list command
list_parser = subparsers.add_parser(
    'list', help='List contents')
list_parser.add_argument(
    'dirname', action='store',
    help='Directory to list')

# A create command
create_parser = subparsers.add_parser(
    'create', help='Create a directory')
create_parser.add_argument(
    'dirname', action='store',
    help='New directory to create')
create_parser.add_argument(
    '--read-only', default=False, action='store_true',
    help='Set permissions to prevent writing to the directory',
)

# A delete command
delete_parser = subparsers.add_parser(
    'delete', help='Remove a directory')
delete_parser.add_argument(
    'dirname', action='store', help='The directory to remove')
delete_parser.add_argument(
    '--recursive', '-r', default=False, action='store_true',
    help='Remove the contents of the directory, too',
)

print(parser.parse_args())
```
```sh
$ python3 argparse_subparsers.py -h

usage: argparse_subparsers.py [-h] {list,create,delete} ...

positional arguments:
  {list,create,delete}  commands
    list                List contents
    create              Create a directory
    delete              Remove a directory

optional arguments:
  -h, --help            show this help message and exit
 
$ python3 argparse_subparsers.py create -h

usage: argparse_subparsers.py create [-h] [--read-only] dirname

positional arguments:
  dirname      New directory to create

optional arguments:
  -h, --help   show this help message and exit
  --read-only  Set permissions to prevent writing to the directo
ry 
```


Reference:  
https://pymotw.com/3/argparse/  
http://www.riptutorial.com/python/example/8571/conceptual-grouping-of-arguments-with-argparse-add-argument-group--

