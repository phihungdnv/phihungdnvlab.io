---
layout: post
title: Install and Deploy an Apache Web Server - Rhel7 [12] - DOCKER
categories: [docker]
view: true
tag: docker
---
`Components:`  
- Build an Apache(httpd) Web server inside container.  
- Expose the service on port 80 of the host  
- Serves a simple index.html file.  
- Display data from a backend server (MariaDB container)  

### DATABASE

**Build database container**
`The procedure in this topic does the following:`  
- Builds a MariaDB server inside docker formatted container  
- Expose the service on port 3306 of the host  
- Start Database service to share a few pieces of information  
- Allows a sript from webser to query the database  

**Get image from registry**
```sh
$ docker pull registry.access.redhat.com/rhel7/rhel
```
**Make a workspace in the /home/$USER/app/maria_database_container**
```sh
$ mkdir /home/$USER/app/maria_database_container
$ cd /home/$USER/app/maria_database_container
```
**Create `Dockerfile`. Here are the contents in that file:**
```
FROM f5ea21241da8
USER root

MAINTAINER phihung.ngo <phihungdnv@gmail.com>
# Update image
RUN yum update -y --disablerepo=*-eus-* --disablerepo=*-htb-* --disablerepo=*sjis* \
    --disablerepo=*-ha-* --disablerepo=*-rt-* --disablerepo=*-lb-* \
    --disablerepo=*-rs-* --disablerepo=*-sap-*

RUN yum-config-manager --disable *-eus-* *-htb-* *-ha-* *-rt-* *-lb-*\
    *-rs-* *-sap-* *-sjis-* > /dev/null

# Add Mariahdb software
RUN yum -y install net-tools mariadb-server

# Set up Mariahdb database
ADD gss_db.sql /tmp/gss_db.sql
RUN /usr/libexec/mariadb-prepare-db-dir
RUN test -d /var/run/mariadb || mkdir /var/run/mariadb; \
    chmod 0777 /var/run/mariadb; \
    /usr/bin/mysqld_safe --basedir=/usr & \
    sleep 10s && \
    /usr/bin/mysqladmin -u root password 'redhat' && \
    mysql --user=root --password=redhat < /tmp/gss_db.sql && \
    mysqladmin shutdown --password=redhat

# Expose Mysql port 3306
EXPOSE 3306

# Start the service
CMD test -d /var/run/mariadb || mkdir /var/run/mariadb; chmod 0777 /var/run/mariadb;/usr/bin/mysqld_safe --basedir=/usr
```
**Build database server image**
```sh
$ docker build -t img_marida_database:v1 /home/rhel7/app/maria_database_container/
```
**Check images**
```
$ docker images
REPOSITORY                                        TAG                 IMAGE ID            CREATED             SIZE
img_marida_database                               v1                  83a37a6ed389        2 minutes ago       1.13 GB
```

**Start database sercontainer**
```
$ docker run --name cont_mariadb -d -p 3306:3306 83a37a6ed389
```
**Test working**
```sh
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
35e6de2c5c0d        83a37a6ed389        "/bin/sh -c 'test ..."   7 seconds ago       Up 6 seconds        0.0.0.0:3306->3306/tcp   cont_mariadb

$ netstat -antp | grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN      -
```
**Firewall config openport 3306 Mysql**
```sh
# firewall-cmd --permanent --add-port=3306
# firewall-cmd --reload
```


### APACHE
**Get image from registry**

**Create a workspace webservice container**
```sh
$ mkdir -p /home/rhel7/app/web_httpd
$ cd /home/rhel7/app/web_httpd
```

**Create `action` file. Here are the contents of that file:**
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb as mdb
import os

con = mdb.connect(os.getenv('DB_SERVICE_SERVICE_HOST','172.17.0.1'), 'dbuser1', 'redhat', 'gss')

with con:

    cur = con.cursor()
    cur.execute("SELECT MESSAGE FROM atomic_training")

    rows = cur.fetchall()

    print 'Content-type:text/html\r\n\r\n'
    print '<html>'
    print '<head>'
    print '<title>My Application</title>'
    print '</head>'
    print '<body>'

    for row in rows:
        print '<h2>' + row[0] + '</h2>'

    print '</body>'
    print '</html>'

    con.close()
```
`172.17.0.1` is ip of docker0 interface  
Get IP
```
$ ip a | grep docker0 | grep inet | awk '{print $2}'
172.17.0.1/16
```

**Create `Dockerfile` in the `workspace`. Here are the contents of that file:**
```
FROM  f5ea21241da8
USER root

MAINTAINER phihung.ngo <phihungdnv@gmail.com>
# Fix per https://bugzilla.redhat.com/show_bug.cgi?id=1192200
RUN yum -y install deltarpm yum-utils --disablerepo=*-eus-* --disablerepo=*-htb-* *-sjis-*\
    --disablerepo=*-ha-* --disablerepo=*-rt-* --disablerepo=*-lb-* --disablerepo=*-rs-* --disablerepo=*-sap-*

RUN yum-config-manager --disable *-eus-* *-htb-* *-ha-* *-rt-* *-lb-* *-rs-* *-sap-* *-sjis* > /dev/null

# Update image
RUN yum update -y
RUN yum install httpd procps-ng MySQL-python -y

# Add configuration file
ADD action /var/www/cgi-bin/action
RUN echo "PassEnv DB_SERVICE_SERVICE_HOST" >> /etc/httpd/conf/httpd.conf
RUN chown root:apache /var/www/cgi-bin/action
RUN chmod 755 /var/www/cgi-bin/action
RUN echo "The Web Server is Running" > /var/www/html/index.html
EXPOSE 80

# Start the service
CMD mkdir /run/httpd ; /usr/sbin/httpd -D FOREGROUND
```

**Build apache server images**
```sh
$ docker build -t img_web_http:v3 /home/rhel7/app/web_httpd
```
**Check image**
```
[rhel7@localhost web_httpd]$ docker images
REPOSITORY                                        TAG                 IMAGE ID            CREATED             SIZE
img_web_http                                      v3                  68e4f0d449f0        10 minutes ago      1.66 GB
```
**Start apache server container**
```
$ docker run --name cont_httpd_web -d -p 8088:80 68e4f0d449f0
```
**Check container running**
```sh
[rhel7@localhost web_httpd]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
84842782b097        68e4f0d449f0        "/bin/sh -c 'mkdir..."   11 seconds ago      Up 10 seconds                    0.0.0.0:8088->80/tcp     cont_httpd_web
```
**GET Response from webserver**
```sh
[rhel7@localhost web_httpd]$ curl localhost:8088/index.html
The Web Server is Running
```
```sh
[rhel7@localhost web_httpd]$ curl 172.16.20.130:8088/cgi-bin/action
<html>
<head>
<title>My Application</title>
</head>
<body>
<h2>RedHat rocks</h2>
<h2>Success</h2>
</body>
</html>
```