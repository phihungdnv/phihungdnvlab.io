---
layout: post
title: find - Unix/Linux
categories: [linux]
view: true
---
**Expressions**
```
..
-amin n - The file was last accessed n minutes ago
-anewer - The file was last accessed more recently than it was modified
-atime n - The file was last accessed more n days ago
-cmin n - The file was last changed n minutes ago
-cnewer - The file was last changed more recently than the file was modified
-ctime n - The file was last changed more than n days ago
-empty - The file is empty
-executable - The file is executable
-false - Always false
-fstype type - The file is on the specified file system
-gid n - The file belongs to group with the ID n
-group groupname - The file belongs to the named group
-ilname pattern - Search for a symbolic line but ignore case
-iname pattern - Search for a file but ignore case
-inum n - Search for a file with the specified node
-ipath path - Search for a path but ignore case
-iregex expression - Search for a expression but ignore case
-links n - Search for a file with the specified number of links
-lname name - Search for a symbolic link
-mmin n - File's data was last modified n minutes ago
-mtime n - File's data was last modified n days ago
-name name - Search for a file with the specified name
-newer name - Search for a file edited more recently than the file given
-nogroup - Search for a file with no group id
-nouser - Search for a file with no user attached to it
-path path - Search for a path
-readable - Find files which are readable
-regex pattern - Search for files matching a regular expression
-type type - Search for a particular type
-uid uid - Files numeric user id is the same as uid
-user name - File is owned by user specified
-writable - Search for files that can be written to
```
**Time range**  
The file was last changed 10 minutes ago -> 0 minutes
```
find . -type f -cmin -10
```
**From 2 to 6 minutes ago**
```
find /u/bill -amin +2 -amin -6
```
**Find files current folder**
```sh
find . -name game
```
**Ignore the case**
```sh
find /etc/sourelist/ -iname "Abc.log"
```
**Find files with path**
```sh
find /etc/sourelist/ -name "*.log"
```
**find all the files more than 100 days ago**
```sh
find /var/log/ -atime 100
```
**Find Empty Files or Folders**
```sh
find /root/ -type f -empty -> file
find /root/ -type d -empty -> folder
```
**Limit depth of directory traversal**
```sh
$ find ./test -maxdepth 2 -name "*.php"
./test/subdir/how.php
./test/cool.php

$ find ./test -maxdepth 1 -name *.php
./test/cool.php
```
**Invert match**
```sh
$ find ./test -not -name "*.php"
./test
./test/abc.txt
./test/subdir
```
**Combine multiple search criterias**
```
$ find ./test -name 'abc*' ! -name '*.php'
./test/abc.txt
./test/abc
```
**OR operator**
```sh
$ find -name '*.php' -o -name '*.txt'
./abc.txt
./subdir/how.php
./abc.php
./cool.php
```
**Search multiple directories together**
```
$ find ./test ./dir2 -type f -name "abc*"
./test/abc.txt
./dir2/abcdefg.txt
```
**Find files with certain permissions**
```sh
$ find . -type f -perm 0664
./abc.txt
./subdir/how.php
./abc.php
./cool.php
```
**Find readonly files**
```sh
$ find /etc -maxdepth 1 -perm /u=r
/etc
/etc/thunderbird
/etc/brltty
/etc/dkms
/etc/phpmyadmin
```
**Find files owned to particular user**
```sh
$ find . -user bob
.
./abc.txt
./abc
./subdir
./subdir/how.php
./abc.php
$ find . -user bob -name '*.php'
```
**Search files belonging to group**
```
find /var/www -group developer
```
**Find files of given size**
```sh
find / -size 50M
```
**Find files in a size range**
```sh
$ find / -size +50M -size -100M
```
**Find largest and smallest files**
```sh
$ find . -type f -exec ls -s {} \; | sort -n -r | head -5    <largest>
$ find . -type f -exec ls -s {} \; | sort -n | head -5  <smallest>
```
**Find 5 file largest >100M**
```sh
 find /home/ -type f -size +100000k -exec ls -sh {} \;| sort -r | head -5
 find /home/ -type f -size +100000k -exec ls -sh {} \;| sort -r |awk '{print $2" with size "$1}' | head -5
```
**Yes/No question**
```sh
find $HOME/. -name *.txt -ok rm {} \;
```
