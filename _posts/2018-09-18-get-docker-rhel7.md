---
layout: post
title: Get Docker for Red Hat Enterprise Linux [1]- DOCKER
categories: [docker]
view: true
tag: docker
---

1. Uninstall old Docker versions
```sh
[rhel7@localhost ~]$ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine \
                  docker-ce
```
2. Set up the repository
```sh
# rm /etc/yum.repos.d/docker*.repo
# subscription-manager repos --enable=rhel-7-server-rpms
# subscription-manager repos --enable=rhel-7-server-extras-rpms
# subscription-manager repos --enable=rhel-7-server-optional-rpms
```
3. Install Docker
```
# yum install docker device-mapper-libs device-mapper-event-libs -yum
```
4. Enable, Start, Status docker
```
# systemctl enable docker.service
# systemctl start docker.service
# systemctl status docker.service
```




