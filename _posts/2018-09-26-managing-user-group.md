---
layout: post
title:  Managing users and group - RHEL 7
categories: [RedHat7]
view: true
---
>Red Hat Enterprise Linux reserves user and group IDs below 1000 for system users and groups.

**Adding a New User**
```sh
# useradd [options] <username>
```
**Common useradd command-line options**
```
-c 'comment' -> example fullname of a useradd
-d <home_directory> -> home directory
-e <YYYY-MM-DD> ->  date for account to be disabled
-g <group name | group number> -> assign to group
-G <group list> -> assign more groups
-m -> create the home directory if it does not exist
-M -> don't create home directory
-p <password>
```
**Assign a password**
```sh
# passwd <username>
```
**Example create user**
```sh
$ sudo useradd -c 'phi hung' -m -p 123456 phihung
$ sudo useradd -m -p 123456 -c 'duong ngoc nhan'  ngocnhan
$ sudo useradd -m -p 123456 -c 'tran cong duy'  congduy
$ sudo useradd -m -p 123456 -c 'vo quoc cuong'  quoccuong
$ sudo useradd -m -p 123456 -c 'nguyen trung hieu'  trunghieu
$ sudo useradd -m -p 123456 -c 'nguyen thi mai' mainguyen
$ sudo useradd -m -p 123456 -c 'tran my ngoc' myngoc
$ sudo useradd -m -p 123456 -c 'thao nhi' thaonhi
$ sudo useradd -m -p 123456 -c 'duong cong son' congson
$ sudo useradd -m -p 123456 -c 'doan the linh' thelinh
```
```sh
[rhel7@172 ~]$ ll /home
total 4
drwx------.  3 congduy   congduy     78 Sep 26 22:59 congduy
drwx------.  3 congson   congson     78 Sep 26 23:03 congson
drwx------.  3 mainguyen mainguyen   78 Sep 26 23:00 mainguyen
drwx------.  3 myngoc    myngoc      78 Sep 26 23:02 myngoc
drwx------.  3 ngocnhan  ngocnhan    78 Sep 26 22:58 ngocnhan
drwx------.  3 phihung   phihung     78 Sep 26 22:57 phihung
drwx------.  3 quoccuong quoccuong   78 Sep 26 22:59 quoccuong
drwx------. 25 rhel7     rhel7     4096 Sep 26 18:36 rhel7
drwx------.  3 thaonhi   thaonhi     78 Sep 26 23:02 thaonhi
drwx------.  3 thelinh   thelinh     78 Sep 26 23:03 thelinh
drwx------.  3 trunghieu trunghieu   78 Sep 26 22:59 trunghieu
```
**List all user UID > 1000**
```sh
[rhel7@172 home]$ cat /etc/passwd | grep '10[0-9][0-9]'
rhel7:x:1000:1000:rhel7:/home/rhel7:/bin/bash
phihung:x:1001:1002:phi hung:/home/phihung:/bin/bash
ngocnhan:x:1002:1003:duong ngoc nhan:/home/ngocnhan:/bin/bash
congduy:x:1003:1004:tran cong duy:/home/congduy:/bin/bash
quoccuong:x:1004:1005:vo quoc cuong:/home/quoccuong:/bin/bash
trunghieu:x:1005:1006:nguyen trung hieu:/home/trunghieu:/bin/bash
mainguyen:x:1006:1007:nguyen thi mai:/home/mainguyen:/bin/bash
myngoc:x:1007:1008:tran my ngoc:/home/myngoc:/bin/bash
thaonhi:x:1008:1009:thao nhi:/home/thaonhi:/bin/bash
congson:x:1009:1010:duong cong son:/home/congson:/bin/bash
thelinh:x:1010:1011:doan the linh:/home/thelinh:/bin/bash
```

**Adding a New Group**
```sh
# groupadd [options] <group_name>
```
**Common groupadd command-line options**
```
-f --force -> try create
...
```
**Example create group**
```sh
sudo groupadd cntt
sudo groupadd ketoan
sudo groupadd kinhdoanh
sudo groupadd developer
```
**List all group**
```sh
$ sudo cat /etc/group
$ sudo cat /etc/group | cut -d : -f 1
```
**List all group with GID > 1000**
```sh
[rhel7@172 home]$ cat /etc/group | grep '10[0-9][0-9]'
rhel7:x:1000:
docker:x:1001:rhel7
phihung:x:1002:
ngocnhan:x:1003:
congduy:x:1004:
quoccuong:x:1005:
trunghieu:x:1006:
mainguyen:x:1007:
myngoc:x:1008:
thaonhi:x:1009:
congson:x:1010:
thelinh:x:1011:
cntt:x:1012:
ketoan:x:1013:
kinhdoanh:x:1014:
developer:x:1015:
```

**Adding an Existing User to an Existing Group**
`override` user's primary group:
```sh
# usermod -g <group_name> <username>
```
`override supplementary` group:
```sh
# usermod -G <group1> <group2> <groupN> <username>
```
`append supplementary` user's group:
```sh
# usermod -aG <group1> <group2> <groupN> <username>
```
**Add multi user to a group**
Example1: add all user to `workspace_sharing` group:
```sh
$ sudo for user in {rhel7,phihung,ngocnhan,congduy,quoccuong,trunghieu,mainguyen,myngoc,thaonhi,congson,thelinh}; do usermod -aG workspace_sharing $user; done
```
Example2: add `phihung`, `quoccuong`, `trunghieu` to `cntt` group
```sh
$ sudo for user in {rhel7,phihung,quoccuong,trunghieu}; do usermod -aG cntt $user; done
```
**List all members of a group**
```sh
$ grep 'workspace_sharing' /etc/group
workspace_sharing:x:1016:rhel7,phihung,ngocnhan,congduy,quoccuong,trunghieu,mainguyen,myngoc,thaonhi,congson,thelinh

$ grep 'cntt' /etc/group
cntt:x:1012:rhel7,phihung,quoccuong,trunghieu
```

**Creating Group Directories**

Example4: A group of people need to work on files in the `/opt/all_member/` directory. All people can access, as `root` user
```sh
$ sudo mkdir /opt/all_member
$ sudo chmod 770 /opt/all_member/
$ sudo ls -l /opt
drwxrwx---. 2 root root  6 Sep 27 00:24 all_member
```
All user not in `root` group can't access this directory
```sh
[phihung@172 home]$ cd /opt/all_member/
bash: cd: /opt/all_member/: Permission denied
```
Associate the contents of the `/opt/all_member/` directory with the `workspace_sharing` group:
```sh
$ sudo chown :workspace_sharing /opt/all_member
$ sudo ls -l /opt/
drwxrwx---. 2 root workspace_sharing  6 Sep 27 00:24 all_member
```
Try to access again with `phihung` user
```sh
[root@172 home]# su phihung
[phihung@172 home]$ cd /opt/all_member/
[phihung@172 all_member]$
```





