---
layout: post
title:  Variables - Shell
categories: [shell]
view: true
---
```sh
#!/usr/bin/env bash
name='Phi Hung'
printf "Hi, %s\n" "$name"
echo "Hi, $name"
```
**Executed shell**
```sh
$ bash script.sh
Hi, Phi Hung
Hi, Phi Hung
```
`Note that spaces cannot be used arount the = assignment operator`  
 
**Argument**
```sh
#!/usr/bin/env bash
echo "Hi, $1"
```
**Execute/Run**
```sh
$ bash shell.sh 'Phi Hung'
Hi, Phi Hung
```
  