---
layout: post
title: Backing Up a Database Running in a Container - Rhel7 [14] - DOCKER
categories: [docker]
view: true
tag: docker
---

### OPTION1: Without mount volume
```sh
$ docker exec mysqlwp mysqldump --all-databases \
--password=wordpressdocker > $HOME/wordpress.backup
```

### OPTION2: Mount volume
```sh
$ docker run --name mysqlwp -e MYSQL_ROOT_PASSWORD=wordpressdocker \
-e MYSQL_DATABASE=wp_database \
-e MYSQL_USER=phihung \
-e MYSQL_PASSWORD=wordpresspwd \	
-v $HOME:/var/lib/mysql \
-d mysql
```