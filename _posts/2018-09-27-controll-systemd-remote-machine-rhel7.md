---
layout: post
title:  Controlling systemd on a remote machine  - RHEL 7
categories: [RedHat7]
view: true
tag: systemctl
---
>systemctl utility also allows you to interact with systemd running on a remote machine over the SSH protocol

**Command**
```sh
$ sudo systemctl --host|-H user_name@host_name command
```
**Example**
```sh
$ sudo systemctl -H root@server-01.example.com status httpd.service
```