---
layout: post
title: Range date - Unix/Linux
categories: [linux]
view: true
---
```sh
[hung.ngo@con01.di01.sapp ~]$ date +'%Y-%m-%d'
2018-09-09
```

```sh
[hung.ngo@con01.di01.sapp ~]$ date +'%m/%d/%Y'
09/09/2018
```
**10 day ago**
```sh
[hung.ngo@con01.di01.sapp ~]$ date -d "10 day ago" +'%Y-%m-%d'
2018-08-30
```
```sh
for i in {1..10}
do
	ls log.$(date -d "$i day ago" + "%Y-%m-%d").gz
done
```