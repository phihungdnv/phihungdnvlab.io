---
layout: post
title:  SHELL - continue and break
categories: [shell]
view: true
---
### continue  
Example for continue
```sh
#! /bin/bash
for i in [series]
do
	command1
	command2
	if (condition)
		continue
	fi
		command3
done
```
### break  
Example for break
```sh
#! /bin/bash
for i in [series]
do 
	command1
	if (condition)
	then
		command2
		break
	fi
		command3
done
```