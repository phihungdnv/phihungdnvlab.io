---
layout: post
title: Slack Developer Kit for Python
categories: [python]
view: true
tag: slack
--- 
# Installation  
```sh
$ virtualenv python_slack
$ source python_slack/bin/activate
$ pip install slackclient
```
# Tokens & Authentication  

- Legacy token generator: https://api.slack.com/custom-integrations/legacy-tokens  
- Bot User OAuth Access Token (App)

## Sending a message to `Channel`  
```python
from slackclient import SlackClient

# Get app_token User OAuth Access Token (App)
slack_app_token = 'xoxb-511248655906-511150047267-ZdUBxWuFW6C7De9cCM5hQdwl'

# Get channel id: https://pycode-workspace.slack.com/messages/CF2K4CGLW/details/
slack_channel_id = 'CF2K4CGLW'

sc = SlackClient(slack_app_token)

message = 'Hi, Phi Hung'

sc.api_call(
  "chat.postMessage",
  channel=slack_channel_id,
  attachments=[
    {   
        "color": "#36a64f",
        "pretext": "Hi @channel", 
        "author_name": "Phi Hung",
        "title": "Slack API attachment by Json",
        "title_link": "http://fb.com/nph249",
        "text": "text-world \n xxx",
        "fields":[
            {
                "title": "Python Level",
                "value": "Good",
            }
        ],
        "footer": "pythonAPI"
    }]
)
```
### `chat.postMessage`

Document: https://api.slack.com/methods/chat.postMessage  
Descriptions: Sends a message to a channel.   

**Arguments**
- channel: channel id `require`  
- text: some messages `require`  
- username: set your bot's user name `optional`  
- attachments: A JSON-based array of structured attachments, presented as a URL-encoded string `optional`.    

### `chat.postEphemeral`   

Sends an ephemeral message to a user in a channel.   
General userid: https://api.slack.com/methods/chat.postEphemeral/test   
```python
sc.api_call(
  "chat.postEphemeral",
  channel=slack_channel_id,
  text="Hello from Python! :tada:",
  user=hung_ngo_id  
)
```


