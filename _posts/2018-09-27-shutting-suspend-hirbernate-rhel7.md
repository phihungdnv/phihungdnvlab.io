---
layout: post
title:  Shutting down, suspending, hibernating the system - RHEL 7
categories: [RedHat7]
view: true
tag: systemctl
---
**Power Management Commands with systemctl**
```
$ sudo systemctl halt			-> Halts the system
$ sudo systemctl poweroff 		-> Powers off the system
$ sudo systemctl reboot			-> Restart system
$ sudo systemctl suspend		-> Suspends system
$ sudo systemctl hibernate		-> Hibernates system
$ sudo systemctl hybird-sleep   -> Hibernates and suspends the system
```
**Shut down the system and power off the machine at a certain time**
```sh
$ sudo shutdown --poweroff hh:mm
```
Where `hh:mm` is the time in 24 hour clock format
**To shut down and halt the system after a delay, without powering off the machine**
```sh
$ sudo shutdown --halt +m
```
Where `+m` is the delay time in minutes. The `now` keyword is an alias for `+0`.
**A pending shutdown can be canceled by the root user as follows:**
```sh
$ sudo shutdown -c
```